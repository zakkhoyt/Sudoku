@testable import MacUtils
import XCTest

final class MacUtilsWrapperTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MacUtilsWrapper().text, "Hello, World!")
    }
}
