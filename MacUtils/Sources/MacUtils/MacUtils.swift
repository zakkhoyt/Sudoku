public struct MacUtilsWrapper {
    public private(set) var text = "Hello, World!"

    public init() {}

    func someFunc(
        p1: [Int],
        p2 _: String,
        p3 _: UInt,
        completion: ([Int]) -> Void
    ) {
        completion(Array(p1))
    }
}
