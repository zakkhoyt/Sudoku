// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// swiftlint:disable prefixed_toplevel_constant
// swiftformat:disable prefixed_toplevel_constant

let package = Package(
    name: "MacUtils",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MacUtils",
            targets: ["MacUtils"]
        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/shibapm/Komondor.git", from: "1.0.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "MacUtils",
            dependencies: []
        ),
        .testTarget(
            name: "MacUtilsTests",
            dependencies: ["MacUtils"]
        )
    ]
)

#if canImport(PackageConfig)
    import PackageConfig

    let config = PackageConfiguration([
        "komondor": [
            // "pre-push": "swift test",
            "pre-commit": [
                "echo Hello from Komondor",
                "swift run swiftformat ../ --config ../.swiftformat",
                "swift run swiftlint --config ../.swiftlint.yml",
                "git add ."
            ]
        ]
    ]).write()
#endif
