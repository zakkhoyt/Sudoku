//
//  QQWingWrapper.m
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/1/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import "QQWingWrapper.h"
#import "QQWing.hpp"

using namespace qqwing;

NSString* generate() {
    SudokuBoard::PrintStyle printStyle= SudokuBoard::ONE_LINE;
    SudokuBoard *board = new SudokuBoard();
    board->setPrintStyle(printStyle);
    board->generatePuzzle();
    board->solve();
    board->printSolution();
    string solution = board->returnSolution();
    NSString *s = [NSString stringWithCString:solution.c_str() encoding:NSUTF8StringEncoding];
    return s;
}


@implementation QQWingWrapper

+(void)generateSolutionWithCompletion:(QQWingWrapperSolutionBlock)completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *solution = generate();
        NSLog(@"%@", solution);
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(solution);
        });
    });
}




@end
