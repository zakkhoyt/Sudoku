//
//  QQWingWrapper.h
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/1/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^QQWingWrapperSolutionBlock)(NSString *solution);

@interface QQWingWrapper : NSObject
+(void)generateSolutionWithCompletion:(QQWingWrapperSolutionBlock)completion;
@end
