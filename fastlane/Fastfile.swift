// This file contains the fastlane.tools configuration
// You can find the documentation at https://docs.fastlane.tools
//
// For a list of all available actions, check out
//
//     https://docs.fastlane.tools/actions
//

import Foundation

class Fastfile: LaneFile {
	func customLane() {
        desc("Description of what the lane does")
		// add actions here: https://docs.fastlane.tools/actions
        log(message: "Printing from fastlane swift \(#function)")
        
	}
    
    func myLane() {
        desc("Description of what the lane does")
        // add actions here: https://docs.fastlane.tools/actions
        
//        print("Printing from fastlane swift")
        logger.verbose(message: "Printing from fastlane swift \(#function)")
        logger.log(message: "Printing from fastlane swift \(#function)")
        log(message: "log from fastlane swift \(#function)")
        echo(message: "echo from fastlane swift \(#function)")
        println(message: "println from fastlane swift \(#function)")
        incrementBuildNumber(xcodeproj: "Sudoku-iOS.xcodeproj")
    }
}
