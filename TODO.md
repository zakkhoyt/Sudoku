
## SudokuKit
* Unit tests
* Refactor to functional

## Sudoku-iOS

### Usability
//* appearnace
//* Quit a game
* finished numbers do not look disabled. 

### Modernize
//* Toolbar / easy reach
* Dark/Light mode
* Particle Emitters

### Performance
* Pre-load audio files



JSON encoding
NumberMode/Mode/NotesMode

# Sept 2022

Change settings mid game
Game saves/resume when app BG/FG

### Highlighting
Consider fade instead of blend (highlight)
Consider RGB add blending (highlight)
Consider flat (highlight)




--- 

# From Notes.app

Sudoku

Basics
- [ ] SudokuKit refactor with tests
- [ ] Quick publish update 
- [ ] SwiftUI
- [ ] Landscape
- [ ] Codable support
- [ ] Mac support
- [ ] Multiplayer support via multi peer wrapper
- [x] Up to date repo
- [ ] Multi platform app

Audio
- [ ] Allow mix with system
- [ ] Use synth kit?
- [ ] Mute/settings 

Settings
- [ ] Options at any time
- [ ] ArgKit

Notes
- [ ] Selective notes
- [ ] Clear all notes for current number 
- [ ] Clear all notes 
- [ ] How to render notes

Gameplay
- [ ] Tap board to select cell, then tap number from memu
    - [ ] Multi select in notes mode
    - [ ] Filter num memu when notes cell selected
- [ ] Game over if only 1 left
- [ ] Zoom on grid?
- [ ] Improve current number selection (wheel or list)
    - [ ] Hide or fade completed
- [ ] Num count remaining 
- [ ] Celebrate when Row/col/grid:num complete

Help
- [ ] Row/col/grid highlights to show completed
    - [ ] Toggle each row col grid
    - [ ] RGB?
    - [ ] Faded highlight if num complete
- [ ] Hint/tip if stuck
- [ ] Tutorial



Settings
- [ ] Render Skint
- [ ] Puzzle generation (depopulate vs qqwing)
- [ ] Color themes


Board
- [ ] Menus access in game
    - [ ] Number
    - [ ] Note mode
    - [ ] Note style
- [ ] Select many cells by touch
- [ ] Tap to select/deselect
- [ ] Long tap for num menu
- [ ] Keyboard support (numbers, delete, shift modifiers)
    - [ ] iPad keyboard menu
    - [ ] Catalyst vs SDFI
- [ ] Notes in corners
￼
￼
    - [ ] https://www.youtube.com/watch?v=QgkVz9sdHEs
    - [ ] Normal
    - [ ] Center
    - [ ] Color
    - [ ] Double
    - [ ] Delete


Unit Tests
- [ ] Run on M1
- [ ] TestPlan

- [ ] Played plain format
- [ ] Sound takeover problem (play a podcast with the game to see)
- [ ] Touch a cell
    - [ ] Highlight played cell
        - [ ] Played cell’s of same #
        - [ ] Matching notes
    - [ ] Highlight empty cell
        - [ ] Red highlight played in RCS

Multiplatform
- [ ] Set up carts on ZBM
- [ ] User defaults refactor
    - [ ] Save board state
    - [ ] Load board state
- [ ] Settings UI
- [ ] Can play game with SwiftUI only
- [ ] Refactor to MP
    - [ ] Fastlane
    - [ ] Linters
    - [ ] Packages
- [ ] New package for app code
- [ ] Keyboard support
    - [ ] Hot keys
    - [ ] Menu items in Mac
    - [ ] Context menu on iOS

IOS app extensions
- [ ] Home Screen widget for current game

macOS extension
- [ ] System tray 

---
 # From Notes.app (chicago)

 Chicago trip

- [ ] Midi game Music
    - [ ] Meow
    - [ ] Jk2
    - [ ] Yellow gold
- [X] ~~*Allow music while playing*~~ [2023-09-18] 
- [X] ~~*User defined hint alpha*~~ [2023-09-18]
- [ ] Hints hard to see with notes
- [ ] Settings while playing
    - [ ] Enable disable notes
    - [ ] Tips
        - [ ] Style
        - [ ] Alpha 
- [ ] Persist game state
- [ ] Difficulty -> tips
- [ ] Leaderboard?
- [ ] Responsiveness (snappy)
  - [ ] I noticed that playing cells (prod app) can be sluggish on tap .
- [ ] Sound effects
  - [ ] better responsiveness (preloading sounds)
  - [ ] trim off leading silence
- [ ] Tutorial
- [ ] Improve hints
    - [ ] Only 1 empty in a rcs after accounting for hints (render a border)
    - [ ] Cycle rcs animated
    - [ ] Hint currency
- [ ] Prev/next buttons for num

* [ ] Responsive audio. 
  * `Important For more advanced playback capabilities, like playing streaming or positional audio, use AVAudioEngine instead.`
* [ ] Separate cell highlight color and BG highlight color
* [ ] Selecting an empty cell is bordered in RED which seems like an error


* [ ] NOtes don't render until re-selecting cell
* [ ] Need to join SudokoBoardPlayResult and Board.ActionMode
  * [ ] Maybe split  `SudokoBoardPlayResult` into `CellDigitStatus` and `CellNoteStatus` which aligns well with `Board.ActionMode`