//
//  AppDelegate.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import SudokuApp
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, AppDelegateCoadjutable {
    var window: UIWindow?
    private let coadjutor = AppDelegateCoadjutor.shared

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        coadjutor.appDelegate = self
        return coadjutor.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
    }
}
