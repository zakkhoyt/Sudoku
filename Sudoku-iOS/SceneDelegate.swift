//
//  SceneDelegate.swift
//
//  Created by Zakk Hoyt on 7/14/21.
//

import SudokuApp
import UIKit

class SceneDelegate: UIResponder, SceneDelegateCoadjutable {
    var window: UIWindow?
    private let coadjutor = SceneDelegateCoadjutor.shared
}

// MARK: - Implements UIWindowSceneDelegate

extension SceneDelegate {
    func windowScene(
        _ windowScene: UIWindowScene,
        didUpdate previousCoordinateSpace: UICoordinateSpace,
        interfaceOrientation previousInterfaceOrientation: UIInterfaceOrientation,
        traitCollection previousTraitCollection: UITraitCollection
    ) {
        // Called when the coordinate space, interface orientation, or trait collection of a UIWindowScene changes
        // Always called when a UIWindowScene moves between screens
        coadjutor.windowScene(
            windowScene,
            didUpdate: previousCoordinateSpace,
            interfaceOrientation: previousInterfaceOrientation,
            traitCollection: previousTraitCollection
        )
    }

    func windowScene(
        _ windowScene: UIWindowScene,
        performActionFor shortcutItem: UIApplicationShortcutItem,
        completionHandler: @escaping (Bool) -> Void
    ) {
        coadjutor.windowScene(
            windowScene,
            performActionFor: shortcutItem,
            completionHandler: completionHandler
        )
    }
}

// MARK: - Implements UISceneDelegate

extension SceneDelegate {
    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        coadjutor.sceneDelegate = self
        coadjutor.scene(
            scene,
            willConnectTo: session,
            options: connectionOptions
        )
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
        coadjutor.sceneDidDisconnect(scene)
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        coadjutor.sceneWillResignActive(scene)
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        coadjutor.sceneDidEnterBackground(scene)
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        coadjutor.sceneWillEnterForeground(scene)
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        coadjutor.sceneDidBecomeActive(scene)
    }
    
    func stateRestorationActivity(
        for scene: UIScene
    ) -> NSUserActivity? {
        // This is the NSUserActivity that will be used to restore state when the Scene reconnects.
        // It can be the same activity used for handoff or spotlight, or it can be a separate activity
        // with a different activity type and/or userInfo.
        // After this method is called, and before the activity is actually saved in the restoration file,
        // if the returned NSUserActivity has a delegate (NSUserActivityDelegate), the method
        // userActivityWillSave is called on the delegate. Additionally, if any UIResponders
        // have the activity set as their userActivity property, the UIResponder updateUserActivityState
        // method is called to update the activity. This is done synchronously and ensures the activity
        // has all info filled in before it is saved.
        coadjutor.stateRestorationActivity(for: scene)
    }
    
    func scene(
        _ scene: UIScene,
        restoreInteractionStateWith stateRestorationActivity: NSUserActivity
    ) {
        // This will be called after scene connection, but before activation, and will provide the
        // activity that was last supplied to the stateRestorationActivityForScene callback, or
        // set on the UISceneSession.stateRestorationActivity property.
        // Note that, if it's required earlier, this activity is also already available in the
        // UISceneSession.stateRestorationActivity at scene connection time.
        coadjutor.scene(
            scene,
            restoreInteractionStateWith: stateRestorationActivity
        )
    }

    func scene(
        _ scene: UIScene,
        willContinueUserActivityWithType userActivityType: String
    ) {
        coadjutor.scene(
            scene,
            willContinueUserActivityWithType: userActivityType
        )
    }

    func scene(
        _ scene: UIScene,
        continue userActivity: NSUserActivity
    ) {
        coadjutor.scene(
            scene,
            continue: userActivity
        )
    }

    func scene(
        _ scene: UIScene,
        didFailToContinueUserActivityWithType userActivityType: String,
        error: Error
    ) {
        coadjutor.scene(
            scene,
            didFailToContinueUserActivityWithType: userActivityType,
            error: error
        )
    }

    func scene(
        _ scene: UIScene,
        didUpdate userActivity: NSUserActivity
    ) {
        coadjutor.scene(
            scene,
            didUpdate: userActivity
        )
    }
}

extension UIViewController {
    var sceneDelegate: SceneDelegate {
        guard let sceneDelegate = scene?.delegate as? SceneDelegate else {
            preconditionFailure("Failed to reference SceneDelegate")
        }
        return sceneDelegate
    }
}
