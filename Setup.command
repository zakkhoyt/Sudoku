#!/bin/bash

# Use the following when you need to debug this as part of an xcode pre-action.
# It will spit the output of this script to a file in the root of the repo.
# Note that xcode doesn't include pre-action script output in the xcode build logs.
#
# exec > ${PROJECT_DIR}/prebuild.log 2>&1

# Gets dir of this script. See https://stackoverflow.com/a/246128/1516011
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Display a message via notification center
notify_error() {
    osascript -e "display notification \"$*\" with title \"Error\""
    sleep 3 # ensure a series of errors has a chance to display one at a time
}

# Installing mint stuff no longer works in macOS Big Sur, at least when brew is installed
# To cope with that pleasantry, we required installation in a custom directory.
#
# We define these as workspace vars to be the following
#
# export MINT_PATH="$HOME/.mint"
# export MINT_LINK_PATH="$MINT_PATH/bin"

# NOTE: If you change these, make sure to change them in MintPaths.xcconfig as well
export MINT_PATH=$HOME/.mint
export MINT_LINK_PATH=$MINT_PATH/bin
export MINT_BIN=$MINT_LINK_PATH

export PATH=$PATH:$MINT_LINK_PATH # NOTE: Doesn't transfer to other build scripts

# Install mint if needed
if ! command -v mint &> /dev/null
then
  osascript -e 'display notification "Mint not installed, installing.  This could take a while ⏳..." with title "Installing dev tools"'
  echo "Mint not installed, installing ⏳..."
  
  # You get some errors if you try to run this script from xcode, unless you
  # do this.
  #
  # NOTE: I tested by echoing SDKROOT before/after this scripts runs via xcode,
  # and this unset doesn't appear to affect the variable outside of this script
  #
  # See https://github.com/yonaskolb/Mint/issues/179.
  unset SDKROOT
  
  TEMP_DIR="$(mktemp -d)"
  echo "NEW DIR: $TEMP_DIR"
  cd $TEMP_DIR
  git clone https://github.com/yonaskolb/Mint.git
  cd Mint
  # lol. Yes, you can use mint to install mint 😅
  swift run mint install yonaskolb/mint
  # rm -rf $TEMP_DIR
  
  cd $SCRIPT_DIR
  
  # Sometimes the environment doesn't have everything needed to install mint
  # Usually, it just means the user needs to install xcode, and run the following command
  # `xcode-select --switch /Applications/Xcode.app/Contents/Developer`
  if ! command -v mint &> /dev/null
  then
    notify_error '"Failed to install mint, do you need to run sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer" with title "Error"'
    echo "Error installing mint.  Is xcode installed and fully set up?\n"
    echo "You might need to run sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer.\n"
    exit 1
  fi
  
  osascript -e 'display notification "Done installing mint. Have a nice day! ✅" with title "Done"'
  osascript -e 'display notification "Additional tools will install using mint..." with title "Wrapping up"'
  echo "Done installing mint. Have a nice day! ✅"
else
  echo "Mint is already installed. Thanks for that! 🎉"
fi

# NOTE: When this script is run from an xcode build, the iOS sdk target can get
# injected, breaking the installation.
# To make sure we're unaffected, it's best to use `xcrun --sdk macosx` in front of each mint command
# for more information, refer to https://github.com/yonaskolb/Mint/issues/179

cd $SCRIPT_DIR

xcrun --sdk macosx mint bootstrap --link || notify_error "Failed running mint bootstrap.  Manually execute Setup.command to see more detail."
# xcrun --sdk macosx mint run swiftgen config run --config ./HatchAssets/swiftgen.yml || notify_error "Failed running swiftgen to generate images, colors, strings in HatchAssets.  Manually execute Setup.command to see more detail."

# For script debugging
#
# say "Done running Setup script"
