// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription
// swiftlint:disable prefixed_toplevel_constant
// swiftformat:disable prefixed_toplevel_constant
let package = Package(
    name: "SudokuKit",
    platforms: [
        .iOS(.v15),
        .macOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SudokuKit",
            targets: ["SudokuKit"]
        ),
        .plugin(
            name: "MySwiftlintPlugin",
            targets: ["MySwiftlintPlugin"]
        ),
//        .plugin(
//            name: "MySwiftformatPlugin",
//            targets: ["MySwiftformatPlugin"]
//        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        // .package(path: "DrawingUI")
        .package(
            name: "DrawingUI",
            path: "../DrawingUI"
        ),

        // https://github.com/realm/SwiftLint/releases
        .package(
            url: "https://github.com/realm/SwiftLint", 
            // https://github.com/realm/SwiftLint/issues/4558
            revision: "0.50.3"
        ),

        // https://github.com/nicklockwood/SwiftFormat/releases
        .package(
            url: "https://github.com/nicklockwood/SwiftFormat", 
            exact: Version(0, 50, 9)
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SudokuKit",
            dependencies: [
                .product(
                    name: "DrawingUI", 
                    package: "DrawingUI"
                )
            ],
            exclude: [
            ],
            resources: [
                .copy("puzzles/pregenerated.csv"),
                .copy("puzzles/pregenerated.json")
            ],
            plugins: [
            //     // https://github.com/realm/SwiftLint/tree/main/Plugins/SwiftLintPlugin
            //    .plugin(
            //         name: "SwiftLintPlugin", 
            //         package: "SwiftLint"
            //     ),
            //     // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
            //     .plugin(
            //         name: "SwiftFormatPlugin",
            //         package: "SwiftFormat"
            //     ),
            //   // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
            //   .plugin(
            //       name: "SwiftFormatPluginXcode",
            //       package: "SwiftFormat"
            //   )
            ]
        ),
        .testTarget(
            name: "SudokuKitTests",
            dependencies: ["SudokuKit"]
        ),
        // https://theswiftdev.com/beginners-guide-to-swift-package-manager-command-plugins/
        // Pros: Control when to format by calling it from command line
        // Cons: 
        //  - Not part of the build process
        //  - Need to do each Package 1 at a time
        // To use:
        // swift package plugin --list
        //   swift package format-source-code
        .plugin(
            name: "MySwiftlintPlugin",
            capability: .command(
                intent: .sourceCodeFormatting(),
                permissions: [
                    .writeToPackageDirectory(reason: "This command reformats source files")
                ]
            ),
            dependencies: [
                .product(
                    name: "swiftlint", 
                    package: "SwiftLint"
                )
            ]
        ),
//        .plugin(
//            name: "MySwiftformatPlugin",
//            capability: .command(
//                intent: .sourceCodeFormatting(),
//                permissions: [
//                    .writeToPackageDirectory(reason: "This command reformats source files")
//                ]
//            ),
//            dependencies: [
//                .product(
//                    name: "swiftformat",
//                    package: "SwiftFormat"
//                )
//            ]
//        )
    ]
)
// swiftformat:enable prefixed_toplevel_constant
// swiftlint:enable prefixed_toplevel_constant
