import CoreGraphics
@testable import SudokuKit
import XCTest

final class SudokuEngineTests: XCTestCase {
    func testGenerateSolutions() {
        let count = 100
        do {
            try PuzzleGenerator.regenerate(count: count)
            print("regenerated \(count) puzzles. Save over file.")
        } catch {
            XCTFail("Failed to regnerate puzzles: \(error)")
        }
    }
}
