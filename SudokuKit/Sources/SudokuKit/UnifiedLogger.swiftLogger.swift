//
//  UnifiedLogger.swift
//
//
//  Created by Zakk Hoyt on 9/18/23.
//

import os

let logger = os.Logger(
    subsystem: "com.vaporwarewolf",
    category: "SudokuKit"
)
