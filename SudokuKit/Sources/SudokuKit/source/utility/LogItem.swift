//
//  LogItem.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/5/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

enum LogType: Int, Codable {
    case given
    case single
    case hiddenSingleRow
    case hiddenSingleColumn
    case hiddenSingleSection
    case guess
    case rollback
    case nakedPairRow
    case nakedPairColumn
    case nakedPairSection
    case pointingPairTripleRow
    case pointingPairTripleColumn
    case rowBox
    case columnBox
    case hiddenPairRow
    case hiddenPairColumn
    case hiddenPairSection
}

public struct LogItem: Codable {
    // Mark Private Variables
    /**
     * The recursion level at which this item was gathered.
     * Used for backing out log items solve branches that
     * don't lead to a solution.
     */
    var round = 0

    /**
     * The type of log message that will determine the
     * message printed.
     */
    var type = LogType.guess

    /**
     * Value that was set by the operation (or zero for no value)
     */
    var value = 0

    /**
     * position on the board at which the value (if any) was set.
     */
    var position = 0

    // MARK: Public methods

    init(round: Int, type: LogType) {
        self.init(round: round, type: type, value: 0, position: -1)
    }

    init(round: Int, type: LogType, value: Int, position: Int) {
        self.round = round
        self.type = type
        self.value = value
        self.position = position
    }

    func getRound() -> Int {
        round
    }

    func print() -> String {
        var output = "Round: \(round) - "

        switch type {
        case .given:
            output += "Mark given"

        case .rollback:
            output += "Roll back round"

        case .guess:
            output += "Mark guess (start round)"

        case .hiddenSingleRow:
            output += "Mark single possibility for value in row"

        case .hiddenSingleColumn:
            output += "Mark single possibility for value in column"

        case .hiddenSingleSection:
            output += "Mark single possibility for value in section"

        case .single:
            output += "Mark only possibility for cell"

        case .nakedPairRow:
            output += "Remove possibilities for naked pair in row"

        case .nakedPairColumn:
            output += "Remove possibilities for naked pair in column"

        case .nakedPairSection:
            output += "Remove possibilities for naked pair in section"

        case .pointingPairTripleRow:
            output += "Remove possibilities for row because all values are in one section"

        case .pointingPairTripleColumn:
            output += "Remove possibilities for column because all values are in one section"

        case .rowBox:
            output += "Remove possibilities for section because all values are in one row"

        case .columnBox:
            output += "Remove possibilities for section because all values are in one column"

        case .hiddenPairRow:
            output += "Remove possibilities from hidden pair in row"

        case .hiddenPairColumn:
            output += "Remove possibilities from hidden pair in column"

        case .hiddenPairSection:
            output += "Remove possibilities from hidden pair in section"
        }
        if value > 0 || position > -1 {
            output += " ("
            var printed = false
            if position > -1 {
//                if printed {
//                    output += " - "
//                }
                output += "Row: \(cellToRow(cell: position) + 1)  - Column: \(cellToColumn(cell: position) + 1)"
                printed = true
            }
            if value > 0 {
                if printed {
                    output += " - "
                }
                output += "Value: \(value)"
                printed = true
            }
            output += ")"
        }
        return output
    }

    func getLogType() -> LogType {
        type
    }
}
