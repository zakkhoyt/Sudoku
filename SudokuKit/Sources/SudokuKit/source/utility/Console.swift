//
//  Logger.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/4/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

let kLogFile = true
let kLogFunction = true
let kLogLine = true

public enum ConsoleType: Int, CustomStringConvertible {
    case info
    case debug
    case warning
    case error
    case todo
    case critical

    public var description: String {
        switch self {
        case .info:
            return "Info"
        case .debug:
            return "Debug"
        case .error:
            return "Error"
        case .warning:
            return "Warning"
        case .todo:
            return "Todo"
        case .critical:
            return "Critical"
        }
    }
}

public class Console: NSObject {
    private class func log(message: String, type: ConsoleType = .info, file: String = #file, function: String = #function, line: Int = #line) {
        let typeString = type.description + "****** - "
        var output = typeString

        if kLogFile {
            let fileURL = NSURL(fileURLWithPath: file)
            if let lastPathComponent = fileURL.lastPathComponent {
                output += lastPathComponent + ":"
            }
        }

        if kLogFunction {
            output += function + ":"
        }

        if kLogLine {
            output += String(line) + ":"
        }

        output += " " + message
        print(output)
    }

    public class func info(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message: message, type: .info, file: file, function: function, line: line)
    }

    public class func debug(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message: message, type: .debug, file: file, function: function, line: line)
    }

    public class func warning(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message: message, type: .warning, file: file, function: function, line: line)
    }

    public class func error(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message: message, type: .error, file: file, function: function, line: line)
    }

    public class func todo(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message: message, type: .todo, file: file, function: function, line: line)
    }

    public class func critical(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(message: message, type: .critical, file: file, function: function, line: line)
        #if DEBUG
            assertionFailure("critical error")
        #endif
    }
}
