//
//  DecodingError+Helpers.swift
//  HatchBrain
//
//  Created by Zakk Hoyt on 7/7/21.
//  Copyright © 2021 Zakk Hoyt. All rights reserved.
//

import Foundation

extension DecodingError: CustomDebugStringConvertible {
    public var subject: String {
        switch self {
        case .typeMismatch(let type, _): return "typeMismatch: \(type)"
        case .valueNotFound(let type, _): return "valueNotFound: \(type)"
        case .keyNotFound(let codingKey, _): return "keyNotFound: \(codingKey)"
        case .dataCorrupted: return "dataCorrupted"
        @unknown default:
            assertionFailure("Developer must add new case for DecodingError.Context")
            return "@unknown"
        }
    }

    public var context: Context {
        switch self {
        case .typeMismatch(_, let context): return context
        case .valueNotFound(_, let context): return context
        case .keyNotFound(_, let context): return context
        case .dataCorrupted(let context): return context
        @unknown default:
            assertionFailure("Developer must add new case for DecodingError.Context")
            return Context(codingPath: [], debugDescription: "@unknown")
        }
    }

    public var debugDescription: String {
        [
            "DecodingError",
            "error: \(localizedDescription)",
            "subject: \(subject)",
            "context: \(context.debugDescription)",
            "codingPath: \(context.codingPath.absoluteCodingPath)",
            {
                guard let underlyingError = context.underlyingError else { return nil }
                return "underlyingError: \(underlyingError.localizedDescription)"
            }()
        ]
        .compactMap { $0 }
        .joined(separator: "\n")
    }
}

extension Collection<CodingKey> {
    /// Represents the coding path as a concise string
    /// I.E. "current.state.desired.current"
    public var absoluteCodingPath: String { map { $0.stringValue }.joined(separator: ".") }
}
