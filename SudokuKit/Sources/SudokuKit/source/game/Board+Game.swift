//
//  Board+Game.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Board {
    func allSectionsValid() -> Bool {
        false
    }

    func allRowsValid() -> Bool {
        false
    }

    func allColumnsValid() -> Bool {
        false
    }

    private func sectionValid(section _: Int) -> Bool {
        false
    }

    private func rowValid(row _: Int) -> Bool {
        false
    }

    private func columnValid(column _: Int) -> Bool {
        false
    }
}
