//
//  Generator.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/5/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//
//  SudokuKit.Generator is a Swift 3.0 port of QQWing availble here: https://github.com/stephenostermiller/qqwing
//  Copyright (C) 2016 Zakk Hoyt (vaporwarewolf@gmail.com)
//  Copyright (C) 2006-2014 Stephen Ostermiller http://ostermiller.org/
//  Copyright (C) 2007 Jacques Bensimon (jacques@ipm.com)
//  Copyright (C) 2011 Jean Guillerez (j.guillerez - orange.fr)
//  Copyright (C) 2014 Michael Catanzaro (mcatanzaro@gnome.org)
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

import Foundation

let kGridSize = 3
let kColumnRowSectionSize = kGridSize * kGridSize
let kSectionGroupSize = kColumnRowSectionSize * kGridSize
let kBoardSize = kColumnRowSectionSize * kColumnRowSectionSize
let kPossibilitySize = kBoardSize * kColumnRowSectionSize

public enum PrintStyle: Int, Codable, CustomStringConvertible {
    case oneLine
    case compact
    case readable
    case csv

    public var description: String {
        switch self {
        case .oneLine:
            return "One Line"
        case .compact:
            return "Compact"
        case .readable:
            return "Readable"
        case .csv:
            return "CSV"
        }
    }
}

public enum Difficulty: Int, CustomStringConvertible, Codable, CaseIterable, Identifiable {
    public var id: String {
        description
    }

    case simple
    case easy
    case intermediate
    case expert
    case unknown

    public var title: String {
        switch self {
        case .simple: "Simple"
        case .easy: "Easy"
        case .intermediate: "Intermediate"
        case .expert: "Expert"
        case .unknown: "Unknown"
        }
    }
    
    public var description: String {
        title
    }
}

public enum Symmetry: Int {
    case none
    case rotate90
    case rotate180
    case mirror
    case flip
    case random
}

public final class Generator {
    public struct Stats: Codable {
        // MARK: Private Variables

        /**
         * The 81 integers that make up a sudoku puzzle.
         * Givens are 1-9, unknowns are 0.
         * Once initialized, this puzzle remains as is.
         * The answer is worked out in "solution".
         */
        public fileprivate(set) var puzzle: [Int] = []

        /**
         * The 81 integers that make up a sudoku puzzle.
         * The solution is built here, after completion
         * all will be 1-9.
         */
        public fileprivate(set) var solution: [Int] = []

        /**
         * Recursion depth at which each of the numbers
         * in the solution were placed. Useful for backing
         * out solve branches that don't lead to a solution.
         */
        public fileprivate(set) var solutionRound: [Int] = []

        /**
         * The 729 integers that make up a the possible
         * values for a Sudoku puzzle. (9 possibilities
         * for each of 81 squares). If possibilities[i]
         * is zero, then the possibility could still be
         * filled in according to the Sudoku rules. When
         * a possibility is eliminated, possibilities[i]
         * is assigned the round (recursion level) at
         * which it was determined that it could not be
         * a possibility.
         */
        public fileprivate(set) var possibilities: [Int] = []

        /**
         * An array the size of the board (81) containing each
         * of the numbers 0-n exactly once. This array may
         * be shuffled so that operations that need to
         * look at each cell can do so in a random order.
         */
        public fileprivate(set) var randomBoardArray: [Int] = []

        /**
         * An array with one element for each position (9), in
         * some random order to be used when trying each
         * position in turn during guesses.
         */
        public fileprivate(set) var randomPossibilityArray: [Int] = []

        /**
         * Whether or not to record history
         */
        public fileprivate(set) var recordHistory = false

        /**
         * Whether or not to print history as it happens
         */
        public fileprivate(set) var logHistory = false

        /**
         * A list of moves used to solve the puzzle.
         * This list contains all moves, even on solve
         * branches that did not lead to a solution.
         */
        public fileprivate(set) var solveHistory: [LogItem] = []

        /**
         * A list of moves used to solve the puzzle.
         * This list contains only the moves needed
         * to solve the puzzle, but doesn't contain
         * information about bad guesses.
         */
        public fileprivate(set) var solveInstructions: [LogItem] = []

        /**
         * The style with which to print puzzles and solutions
         */
        public fileprivate(set) var printStyle = PrintStyle.readable

        /**
         * The last round of solving
         */
        public fileprivate(set) var lastSolveRound = 0
    }

    public private(set) var stats = Stats()

    // MARK: Public methods

    public init() {
        // Create content for arrays
        for i in 0..<kBoardSize {
            stats.puzzle.append(0)
            stats.solution.append(0)
            stats.solutionRound.append(0)
            stats.randomBoardArray.append(i)
        }
        for _ in 0..<kPossibilitySize {
            stats.possibilities.append(0)
        }
        for i in 0..<kColumnRowSectionSize {
            stats.randomPossibilityArray.append(i)
        }
    }

    public func setPuzzle(initPuzzle: [Int]) -> Bool {
        if !initPuzzle.isEmpty {
            for i in 0..<kBoardSize {
                stats.puzzle[i] = initPuzzle[i]
            }
        } else {
            Console.warning("initPuzzle is empty")
        }
        return reset()
    }

    public func printPuzzle() -> String {
        printBoard(sudoku: stats.puzzle)
    }

    public func printSolution() -> String {
        printBoard(sudoku: stats.solution)
    }

    public func solve() -> Bool {
        _ = reset()
        shuffleRandomArrays()
        return solve(round: 2)
    }

    /**
     * Count the number of solutions to the puzzle
     */
    public func countSolutions() -> Int {
        countSolutions(limitToTwo: false)
    }

    /**
     * Count the number of solutions to the puzzle
     * but return two any time there are two or
     * more solutions. This method will run much
     * falter than countSolutions() when there
     * are many possible solutions and can be used
     * when you are interested in knowing if the
     * puzzle has zero, one, or multiple solutions.
     */
    public func countSolutionsLimited() -> Int {
        countSolutions(limitToTwo: true)
    }

    /**
     * return true if the puzzle has a solution
     * and only a single solution
     */
    public func hasUniqueSolution() -> Bool {
        countSolutionsLimited() == 1
    }

    public func isSolved() -> Bool {
        for i in 0..<kBoardSize {
            if stats.solution[i] == 0 {
                return false
            }
        }
        return true
    }

    public func printSolveHistory() -> String {
        printHistory(v: stats.solveHistory)
    }

    public func setRecordHistory(recordHistory: Bool) {
        stats.recordHistory = recordHistory
    }

    public func setLogHistory(logHistory: Bool) {
        stats.logHistory = logHistory
    }

    public func setPrintStyle(printStyle: PrintStyle) {
        stats.printStyle = printStyle
    }

    public func generatePuzzle() {
        generatePuzzleSymmetry(inSymmetry: .none)
    }

    public func generatePuzzleSymmetry(inSymmetry: Symmetry) {
        var symmetry = inSymmetry
        if symmetry == .random {
            symmetry = getRandomSymmetry()
        }

        // Don't record history while generating.
        let recHistory = stats.recordHistory
        setRecordHistory(recordHistory: false)
        let lHistory = stats.logHistory
        setLogHistory(logHistory: false)

        clearPuzzle()

        // Start by getting the randomness in order so that
        // each puzzle will be different from the last.
        shuffleRandomArrays()

        // Now solve the puzzle the whole way. The solve
        // uses random algorithms, so we should have a
        // really randomly totally filled sudoku
        // Even when starting from an empty grid
        _ = solve()

        if symmetry == .none {
            // Rollback any square for which it is obvious that
            // the square doesn't contribute to a unique solution
            // (ie, squares that were filled by logic rather
            // than by guess)
            rollbackNonGuesses()
        }

        // Record all marked squares as the puzzle so
        // that we can call countSolutions without losing it.
        for i in 0..<kBoardSize {
            stats.puzzle[i] = stats.solution[i]
        }

        // Rerandomize everything so that we test squares
        // in a different order than they were added.
        shuffleRandomArrays()

        // Remove one value at a time and see if
        // the puzzle still has only one solution.
        // If it does, leave it out the point because
        // it is not needed.
        for i in 0..<kBoardSize {
            // check all the positions, but in shuffled order
            let position = stats.randomBoardArray[i]
            if stats.puzzle[position] > 0 {
                var positionsym1 = -1
                var positionsym2 = -1
                var positionsym3 = -1
                switch symmetry {
                case .rotate90:
                    positionsym2 = columnRowToCell(
                        column: cellToRow(cell: position),
                        row: kColumnRowSectionSize - 1 - cellToColumn(cell: position)
                    )
                    positionsym3 = columnRowToCell(
                        column: kColumnRowSectionSize - 1 - cellToRow(cell: position),
                        row: cellToColumn(cell: position)
                    )
                case .rotate180:
                    positionsym1 = columnRowToCell(
                        column: kColumnRowSectionSize - 1 - cellToColumn(cell: position),
                        row: kColumnRowSectionSize - 1 - cellToRow(cell: position)
                    )
                case .mirror:
                    positionsym1 = columnRowToCell(
                        column: kColumnRowSectionSize - 1 - cellToColumn(cell: position),
                        row: cellToRow(cell: position)
                    )
                case .flip:
                    positionsym1 = columnRowToCell(
                        column: cellToColumn(cell: position),
                        row: kColumnRowSectionSize - 1 - cellToRow(cell: position)
                    )
                case .random: // NOTE: Should never happen
                    break
                case .none: // NOTE: No need to do anything
                    break
                }
                // try backing out the value and
                // counting solutions to the puzzle
                let savedValue = stats.puzzle[position]
                stats.puzzle[position] = 0
                var savedSym1 = 0
                if positionsym1 >= 0 {
                    savedSym1 = stats.puzzle[positionsym1]
                    stats.puzzle[positionsym1] = 0
                }
                var savedSym2 = 0
                if positionsym2 >= 0 {
                    savedSym2 = stats.puzzle[positionsym2]
                    stats.puzzle[positionsym2] = 0
                }
                var savedSym3 = 0
                if positionsym3 >= 0 {
                    savedSym3 = stats.puzzle[positionsym3]
                    stats.puzzle[positionsym3] = 0
                }
                _ = reset()
                if countSolutions(round: 2, limitToTwo: true) > 1 {
                    // Put it back in, it is needed
                    stats.puzzle[position] = savedValue
                    if positionsym1 >= 0, savedSym1 != 0 {
                        stats.puzzle[positionsym1] = savedSym1
                    }
                    if positionsym2 >= 0, savedSym2 != 0 {
                        stats.puzzle[positionsym2] = savedSym2
                    }
                    if positionsym3 >= 0, savedSym3 != 0 {
                        stats.puzzle[positionsym3] = savedSym3
                    }
                }
            }
        }

        // Clear all solution info, leaving just the puzzle.
        _ = reset()

        // Restore recording history.
        setRecordHistory(recordHistory: recHistory)
        setLogHistory(logHistory: lHistory)
    }

    public func getGivenCount() -> Int {
        var count = 0
        for i in 0..<kBoardSize {
            if stats.puzzle[i] != 0 {
                count += 1
            }
        }
        return count
    }

    public func getSingleCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .single)
    }

    public func getHiddenSingleCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .hiddenSingleRow) +
        getLogCount(v: stats.solveInstructions, type: .hiddenSingleColumn) +
        getLogCount(v: stats.solveInstructions, type: .hiddenSingleSection)
    }

    public func getNakedPairCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .nakedPairRow) +
        getLogCount(v: stats.solveInstructions, type: .nakedPairColumn) +
        getLogCount(v: stats.solveInstructions, type: .nakedPairSection)
    }

    public func getHiddenPairCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .hiddenPairRow) +
        getLogCount(v: stats.solveInstructions, type: .hiddenPairColumn) +
        getLogCount(v: stats.solveInstructions, type: .hiddenPairSection)
    }

    public func getBoxLineReductionCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .rowBox) +
        getLogCount(v: stats.solveInstructions, type: .columnBox)
    }

    public func getPointingPairTripleCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .pointingPairTripleRow) +
        getLogCount(v: stats.solveInstructions, type: .pointingPairTripleColumn)
    }

    public func getGuessCount() -> Int {
        getLogCount(v: stats.solveInstructions, type: .guess)
    }

    public func getBacktrackCount() -> Int {
        getLogCount(v: stats.solveHistory, type: .rollback)
    }

    public func printSolveInstructions() -> String {
        if isSolved() {
            return printHistory(v: stats.solveInstructions)
        } else {
            // cout << "No solve instructions - Puzzle is not possible to solve." << endl
            let output = "No solve instructions - Puzzle is not possible to solve.\n"
            print(output)
            return output
        }
    }

    public var difficulty: Difficulty {
        if getGuessCount() > 0 {
            return .expert
        }
        if getBoxLineReductionCount() > 0 {
            return .intermediate
        }
        if getPointingPairTripleCount() > 0 {
            return .intermediate
        }
        if getHiddenPairCount() > 0 {
            return .intermediate
        }
        if getNakedPairCount() > 0 {
            return .intermediate
        }
        if getHiddenSingleCount() > 0 {
            return .easy
        }
        if getSingleCount() > 0 {
            return .simple
        }
        return .unknown
    }

    // MARK: Private methods

    deinit {
        clearPuzzle()
    }

    func reset() -> Bool {
        for i in 0..<kBoardSize {
            stats.solution[i] = 0
        }

        for i in 0..<kBoardSize {
            stats.solutionRound[i] = 0
        }

        for i in 0..<kPossibilitySize {
            stats.possibilities[i] = 0
        }

        stats.solveHistory.removeAll()
        stats.solveInstructions.removeAll()

        let round = 1
        for position in 0..<kBoardSize {
            if stats.puzzle[position] > 0 {
                let valIndex = stats.puzzle[position] - 1
                let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                let value = stats.puzzle[position]
                if stats.possibilities[valPos] != 0 {
                    return false
                }
                mark(position: position, round: round, value: value)
                if stats.logHistory || stats.recordHistory {
                    addHistoryItem(
                        logItem: LogItem(
                            round: round,
                            type: .given,
                            value: value,
                            position: position
                        )
                    )
                }
            }
        }
        return true
    }

    func singleSolveMove(round: Int) -> Bool {
        if onlyPossibilityForCell(round: round) {
            return true
        }
        if onlyValueInSection(round: round) {
            return true
        }
        if onlyValueInRow(round: round) {
            return true
        }
        if onlyValueInColumn(round: round) {
            return true
        }
        if handleNakedPairs(round: round) {
            return true
        }
        if pointingRowReduction(round: round) {
            return true
        }
        if pointingColumnReduction(round: round) {
            return true
        }
        if rowBoxReduction(round: round) {
            return true
        }
        if colBoxReduction(round: round) {
            return true
        }
        if hiddenPairInRow(round: round) {
            return true
        }
        if hiddenPairInColumn(round: round) {
            return true
        }
        if hiddenPairInSection(round: round) {
            return true
        }
        return false
    }

    func onlyPossibilityForCell(round: Int) -> Bool {
        for position in 0..<kBoardSize {
            if stats.solution[position] == 0 {
                var count = 0
                var lastValue = 0
                for valIndex in 0..<kColumnRowSectionSize {
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        count += 1
                        lastValue = valIndex + 1
                    }
                }
                if count == 1 {
                    mark(position: position, round: round, value: lastValue)
                    if stats.logHistory || stats.recordHistory {
                        addHistoryItem(logItem: LogItem(round: round, type: .single, value: lastValue, position: position))
                    }
                    return true
                }
            }
        }
        return false
    }

    func onlyValueInRow(round: Int) -> Bool {
        for row in 0..<kColumnRowSectionSize {
            for valIndex in 0..<kColumnRowSectionSize {
                var count = 0
                var lastPosition = 0
                for col in 0..<kColumnRowSectionSize {
                    let position = (row * kColumnRowSectionSize) + col
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        count += 1
                        lastPosition = position
                    }
                }
                if count == 1 {
                    let value = valIndex + 1
                    if stats.logHistory || stats.recordHistory {
                        addHistoryItem(logItem: LogItem(round: round, type: .hiddenSingleRow, value: value, position: lastPosition))
                    }
                    mark(position: lastPosition, round: round, value: value)
                    return true
                }
            }
        }
        return false
    }

    func onlyValueInColumn(round: Int) -> Bool {
        for col in 0..<kColumnRowSectionSize {
            for valIndex in 0..<kColumnRowSectionSize {
                var count = 0
                var lastPosition = 0
                for row in 0..<kColumnRowSectionSize {
                    let position = columnRowToCell(column: col, row: row)
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        count += 1
                        lastPosition = position
                    }
                }
                if count == 1 {
                    let value = valIndex + 1
                    if stats.logHistory || stats.recordHistory {
                        addHistoryItem(
                            logItem: LogItem(
                                round: round,
                                type: .hiddenSingleColumn,
                                value: value,
                                position: lastPosition
                            )
                        )
                    }
                    mark(position: lastPosition, round: round, value: value)
                    return true
                }
            }
        }
        return false
    }

    func onlyValueInSection(round: Int) -> Bool {
        for sec in 0..<kColumnRowSectionSize {
            let secPos = sectionToFirstCell(section: sec)
            for valIndex in 0..<kColumnRowSectionSize {
                var count = 0
                var lastPosition = 0
                for i in 0..<kGridSize {
                    for j in 0..<kGridSize {
                        let position = secPos + i + kColumnRowSectionSize * j
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                        if stats.possibilities[valPos] == 0 {
                            count += 1
                            lastPosition = position
                        }
                    }
                }
                if count == 1 {
                    let value = valIndex + 1
                    if stats.logHistory || stats.recordHistory {
                        addHistoryItem(logItem: LogItem(round: round, type: .hiddenSingleSection, value: value, position: lastPosition))
                    }
                    mark(position: lastPosition, round: round, value: value)
                    return true
                }
            }
        }
        return false
    }

    /**
     * The last round of solving
     */
    func solve(round: Int) -> Bool {
        stats.lastSolveRound = round

        while singleSolveMove(round: round) {
            if isSolved() {
                return true
            }
            if isImpossible() {
                return false
            }
        }

        let nextGuessRound = round + 1
        let nextRound = round + 2
        var guessNumber = 0
        while guess(round: nextGuessRound, guessNumber: guessNumber) {
            guessNumber += 1
            if isImpossible() || !solve(round: nextRound) {
                rollbackRound(round: nextRound)
                rollbackRound(round: nextGuessRound)
            } else {
                return true
            }
        }
        return false
    }

    func countSolutions(limitToTwo: Bool) -> Int {
        // Don't record history while generating.
        let recHistory = stats.recordHistory
        setRecordHistory(recordHistory: false)
        let lHistory = stats.logHistory
        setLogHistory(logHistory: false)

        _ = reset()
        let solutionCount = countSolutions(round: 2, limitToTwo: limitToTwo)

        // Restore recording history.
        setRecordHistory(recordHistory: recHistory)
        setLogHistory(logHistory: lHistory)

        return solutionCount
    }

    func countSolutions(round: Int, limitToTwo: Bool) -> Int {
        while singleSolveMove(round: round) {
            if isSolved() {
                rollbackRound(round: round)
                return 1
            }
            if isImpossible() {
                rollbackRound(round: round)
                return 0
            }
        }

        var solutions = 0
        let nextRound = round + 1
        var guessNumber = 0
        while guess(round: nextRound, guessNumber: guessNumber) {
            guessNumber += 1

            solutions += countSolutions(round: nextRound, limitToTwo: limitToTwo)
            if limitToTwo, solutions >= 2 {
                rollbackRound(round: round)
                return solutions
            }
        }
        rollbackRound(round: round)
        return solutions
    }

    func guess(round: Int, guessNumber: Int) -> Bool {
        var localGuessCount = 0
        let position = findPositionWithFewestPossibilities()
        for i in 0..<kColumnRowSectionSize {
            let valIndex = stats.randomPossibilityArray[i]
            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
            if stats.possibilities[valPos] == 0 {
                if localGuessCount == guessNumber {
                    let value = valIndex + 1
                    if stats.logHistory || stats.recordHistory {
                        addHistoryItem(logItem: LogItem(round: round, type: .guess, value: value, position: position))
                    }
                    mark(position: position, round: round, value: value)
                    return true
                }
                localGuessCount += 1
            }
        }
        return false
    }

    func isImpossible() -> Bool {
        for position in 0..<kBoardSize {
            if stats.solution[position] == 0 {
                var count = 0
                for valIndex in 0..<kColumnRowSectionSize {
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        count += 1
                    }
                }
                if count == 0 {
                    return true
                }
            }
        }
        return false
    }

    func rollbackRound(round: Int) {
        if stats.logHistory || stats.recordHistory {
            addHistoryItem(logItem: LogItem(round: round, type: .rollback))
        }

        for i in 0..<kBoardSize {
            if stats.solutionRound[i] == round {
                stats.solutionRound[i] = 0
                stats.solution[i] = 0
            }
        }

        for i in 0..<kPossibilitySize {
            if stats.possibilities[i] == round {
                stats.possibilities[i] = 0
            }
        }

        while !stats.solveInstructions.isEmpty, stats.solveInstructions.last?.getRound() == round {
            stats.solveInstructions.removeLast()
        }
    }

    func pointingRowReduction(round: Int) -> Bool {
        for valIndex in 0..<kColumnRowSectionSize {
            for section in 0..<kColumnRowSectionSize {
                let secStart = sectionToFirstCell(section: section)
                var inOneRow = true
                var boxRow = -1
                for j in 0..<kGridSize {
                    for i in 0..<kGridSize {
                        let secVal = secStart + i + (kColumnRowSectionSize * j)
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: secVal)
                        if stats.possibilities[valPos] == 0 {
                            if boxRow == -1 || boxRow == j {
                                boxRow = j
                            } else {
                                inOneRow = false
                            }
                        }
                    }
                }
                if inOneRow, boxRow != -1 {
                    var doneSomething = false
                    let row = cellToRow(cell: secStart) + boxRow
                    let rowStart = rowToFirstCell(row: row)
                    for i in 0..<kColumnRowSectionSize {
                        let position = rowStart + i
                        let section2 = cellToSection(cell: position)
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                        if section != section2, stats.possibilities[valPos] == 0 {
                            stats.possibilities[valPos] = round
                            doneSomething = true
                        }
                    }
                    if doneSomething {
                        if stats.logHistory || stats.recordHistory {
                            addHistoryItem(logItem: LogItem(round: round, type: .pointingPairTripleRow, value: valIndex + 1, position: rowStart))
                        }
                        return true
                    }
                }
            }
        }
        return false
    }

    func rowBoxReduction(round: Int) -> Bool {
        for valIndex in 0..<kColumnRowSectionSize {
            for row in 0..<kColumnRowSectionSize {
                let rowStart = rowToFirstCell(row: row)
                var inOneBox = true
                var rowBox = -1
                for i in 0..<kGridSize {
                    for j in 0..<kGridSize {
                        let column = i * kGridSize + j
                        let position = columnRowToCell(column: column, row: row)
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                        if stats.possibilities[valPos] == 0 {
                            if rowBox == -1 || rowBox == i {
                                rowBox = i
                            } else {
                                inOneBox = false
                            }
                        }
                    }
                }
                if inOneBox, rowBox != -1 {
                    var doneSomething = false
                    let column = kGridSize * rowBox
                    let secStart = cellToSectionStartCell(cell: columnRowToCell(column: column, row: row))
                    let secStartRow = cellToRow(cell: secStart)
                    let secStartCol = cellToColumn(cell: secStart)
                    for i in 0..<kGridSize {
                        for j in 0..<kGridSize {
                            let row2 = secStartRow + i
                            let col2 = secStartCol + j
                            let position = columnRowToCell(column: col2, row: row2)
                            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                            if row != row2, stats.possibilities[valPos] == 0 {
                                stats.possibilities[valPos] = round
                                doneSomething = true
                            }
                        }
                    }
                    if doneSomething {
                        if stats.logHistory || stats.recordHistory {
                            addHistoryItem(logItem: LogItem(round: round, type: .rowBox, value: valIndex + 1, position: rowStart))
                        }
                        return true
                    }
                }
            }
        }
        return false
    }

    func colBoxReduction(round: Int) -> Bool {
        for valIndex in 0..<kColumnRowSectionSize {
            for col in 0..<kColumnRowSectionSize {
                let colStart = columnToFirstCell(column: col)
                var inOneBox = true
                var colBox = -1
                for i in 0..<kGridSize {
                    for j in 0..<kGridSize {
                        let row = i * kGridSize + j
                        let position = columnRowToCell(column: col, row: row)
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                        if stats.possibilities[valPos] == 0 {
                            if colBox == -1 || colBox == i {
                                colBox = i
                            } else {
                                inOneBox = false
                            }
                        }
                    }
                }
                if inOneBox, colBox != -1 {
                    var doneSomething = false
                    let row = kGridSize * colBox
                    let secStart = cellToSectionStartCell(cell: columnRowToCell(column: col, row: row))
                    let secStartRow = cellToRow(cell: secStart)
                    let secStartCol = cellToColumn(cell: secStart)
                    for i in 0..<kGridSize {
                        for j in 0..<kGridSize {
                            let row2 = secStartRow + i
                            let col2 = secStartCol + j
                            let position = columnRowToCell(column: col2, row: row2)
                            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                            if col != col2, stats.possibilities[valPos] == 0 {
                                stats.possibilities[valPos] = round
                                doneSomething = true
                            }
                        }
                    }
                    if doneSomething {
                        if stats.logHistory || stats.recordHistory {
                            addHistoryItem(logItem: LogItem(round: round, type: .columnBox, value: valIndex + 1, position: colStart))
                        }
                        return true
                    }
                }
            }
        }
        return false
    }

    func pointingColumnReduction(round: Int) -> Bool {
        for valIndex in 0..<kColumnRowSectionSize {
            for section in 0..<kColumnRowSectionSize {
                let secStart = sectionToFirstCell(section: section)
                var inOneCol = true
                var boxCol = -1
                for i in 0..<kGridSize {
                    for j in 0..<kGridSize {
                        let secVal = secStart + i + (kColumnRowSectionSize * j)
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: secVal)
                        if stats.possibilities[valPos] == 0 {
                            if boxCol == -1 || boxCol == i {
                                boxCol = i
                            } else {
                                inOneCol = false
                            }
                        }
                    }
                }
                if inOneCol, boxCol != -1 {
                    var doneSomething = false
                    let col = cellToColumn(cell: secStart) + boxCol
                    let colStart = columnToFirstCell(column: col)
                    for i in 0..<kColumnRowSectionSize {
                        let position = colStart + (kColumnRowSectionSize * i)
                        let section2 = cellToSection(cell: position)
                        let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                        if section != section2, stats.possibilities[valPos] == 0 {
                            stats.possibilities[valPos] = round
                            doneSomething = true
                        }
                    }
                    if doneSomething {
                        if stats.logHistory || stats.recordHistory {
                            addHistoryItem(logItem: LogItem(round: round, type: .pointingPairTripleColumn, value: valIndex + 1, position: colStart))
                        }
                        return true
                    }
                }
            }
        }
        return false
    }

    func hiddenPairInRow(round: Int) -> Bool {
        for row in 0..<kColumnRowSectionSize {
            for valIndex in 0..<kColumnRowSectionSize {
                var c1 = -1
                var c2 = -1
                var valCount = 0
                for column in 0..<kColumnRowSectionSize {
                    let position = columnRowToCell(column: column, row: row)
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        if c1 == -1 || c1 == column {
                            c1 = column
                        } else if c2 == -1 || c2 == column {
                            c2 = column
                        }
                        valCount += 1
                    }
                }
                if valCount == 2 {
                    for valIndex2 in (valIndex + 1)..<kColumnRowSectionSize {
                        var c3 = -1
                        var c4 = -1
                        var valCount2 = 0
                        for column in 0..<kColumnRowSectionSize {
                            let position = columnRowToCell(column: column, row: row)
                            let valPos = getPossibilityIndex(valueIndex: valIndex2, cell: position)
                            if stats.possibilities[valPos] == 0 {
                                if c3 == -1 || c3 == column {
                                    c3 = column
                                } else if c4 == -1 || c4 == column {
                                    c4 = column
                                }
                                valCount2 += 1
                            }
                        }
                        if valCount2 == 2, c1 == c3, c2 == c4 {
                            var doneSomething = false
                            for valIndex3 in 0..<kColumnRowSectionSize {
                                if valIndex3 != valIndex, valIndex3 != valIndex2 {
                                    let position1 = columnRowToCell(column: c1, row: row)
                                    let position2 = columnRowToCell(column: c2, row: row)
                                    let valPos1 = getPossibilityIndex(valueIndex: valIndex3, cell: position1)
                                    let valPos2 = getPossibilityIndex(valueIndex: valIndex3, cell: position2)
                                    if stats.possibilities[valPos1] == 0 {
                                        stats.possibilities[valPos1] = round
                                        doneSomething = true
                                    }
                                    if stats.possibilities[valPos2] == 0 {
                                        stats.possibilities[valPos2] = round
                                        doneSomething = true
                                    }
                                }
                            }
                            if doneSomething {
                                if stats.logHistory || stats.recordHistory {
                                    addHistoryItem(
                                        logItem: LogItem(
                                            round: round,
                                            type: .hiddenPairRow,
                                            value: valIndex + 1,
                                            position: columnRowToCell(
                                                column: c1, row: row
                                            )
                                        )
                                    )
                                }
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    func hiddenPairInColumn(round: Int) -> Bool {
        for column in 0..<kColumnRowSectionSize {
            for valIndex in 0..<kColumnRowSectionSize {
                var r1 = -1
                var r2 = -1
                var valCount = 0
                for row in 0..<kColumnRowSectionSize {
                    let position = columnRowToCell(column: column, row: row)
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        if r1 == -1 || r1 == row {
                            r1 = row
                        } else if r2 == -1 || r2 == row {
                            r2 = row
                        }
                        valCount += 1
                    }
                }
                if valCount == 2 {
                    for valIndex2 in (valIndex + 1)..<kColumnRowSectionSize {
                        var r3 = -1
                        var r4 = -1
                        var valCount2 = 0
                        for row in 0..<kColumnRowSectionSize {
                            let position = columnRowToCell(column: column, row: row)
                            let valPos = getPossibilityIndex(valueIndex: valIndex2, cell: position)
                            if stats.possibilities[valPos] == 0 {
                                if r3 == -1 || r3 == row {
                                    r3 = row
                                } else if r4 == -1 || r4 == row {
                                    r4 = row
                                }
                                valCount2 += 1
                            }
                        }
                        if valCount2 == 2, r1 == r3, r2 == r4 {
                            var doneSomething = false
                            for valIndex3 in 0..<kColumnRowSectionSize {
                                if valIndex3 != valIndex, valIndex3 != valIndex2 {
                                    let position1 = columnRowToCell(column: column, row: r1)
                                    let position2 = columnRowToCell(column: column, row: r2)
                                    let valPos1 = getPossibilityIndex(valueIndex: valIndex3, cell: position1)
                                    let valPos2 = getPossibilityIndex(valueIndex: valIndex3, cell: position2)
                                    if stats.possibilities[valPos1] == 0 {
                                        stats.possibilities[valPos1] = round
                                        doneSomething = true
                                    }
                                    if stats.possibilities[valPos2] == 0 {
                                        stats.possibilities[valPos2] = round
                                        doneSomething = true
                                    }
                                }
                            }
                            if doneSomething {
                                if stats.logHistory || stats.recordHistory {
                                    addHistoryItem(
                                        logItem: LogItem(
                                            round: round,
                                            type: .hiddenPairColumn,
                                            value: valIndex + 1,
                                            position: columnRowToCell(
                                                column: column, row: r1
                                            )
                                        )
                                    )
                                }
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    func hiddenPairInSection(round: Int) -> Bool {
        for section in 0..<kColumnRowSectionSize {
            for valIndex in 0..<kColumnRowSectionSize {
                var si1 = -1
                var si2 = -1
                var valCount = 0
                for secInd in 0..<kColumnRowSectionSize {
                    let position = sectionToCell(section: section, offset: secInd)
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        if si1 == -1 || si1 == secInd {
                            si1 = secInd
                        } else if si2 == -1 || si2 == secInd {
                            si2 = secInd
                        }
                        valCount += 1
                    }
                }
                if valCount == 2 {
                    for valIndex2 in (valIndex + 1)..<kColumnRowSectionSize {
                        var si3 = -1
                        var si4 = -1
                        var valCount2 = 0
                        for secInd in 0..<kColumnRowSectionSize {
                            let position = sectionToCell(section: section, offset: secInd)
                            let valPos = getPossibilityIndex(valueIndex: valIndex2, cell: position)
                            if stats.possibilities[valPos] == 0 {
                                if si3 == -1 || si3 == secInd {
                                    si3 = secInd
                                } else if si4 == -1 || si4 == secInd {
                                    si4 = secInd
                                }
                                valCount2 += 1
                            }
                        }
                        if valCount2 == 2, si1 == si3, si2 == si4 {
                            var doneSomething = false
                            for valIndex3 in 0..<kColumnRowSectionSize {
                                if valIndex3 != valIndex, valIndex3 != valIndex2 {
                                    let position1 = sectionToCell(section: section, offset: si1)
                                    let position2 = sectionToCell(section: section, offset: si2)
                                    let valPos1 = getPossibilityIndex(valueIndex: valIndex3, cell: position1)
                                    let valPos2 = getPossibilityIndex(valueIndex: valIndex3, cell: position2)
                                    if stats.possibilities[valPos1] == 0 {
                                        stats.possibilities[valPos1] = round
                                        doneSomething = true
                                    }
                                    if stats.possibilities[valPos2] == 0 {
                                        stats.possibilities[valPos2] = round
                                        doneSomething = true
                                    }
                                }
                            }
                            if doneSomething {
                                if stats.logHistory || stats.recordHistory {
                                    addHistoryItem(
                                        logItem: LogItem(
                                            round: round,
                                            type: .hiddenPairSection,
                                            value: valIndex + 1,
                                            position: sectionToCell(
                                                section: section,
                                                offset: si1
                                            )
                                        )
                                    )
                                }
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    func mark(position: Int, round: Int, value: Int) {
        // TODO: throw and catch from callers
        if stats.solution[position] != 0 {
            assertionFailure("Marking position that already has been marked.")
        }
        if stats.solutionRound[position] != 0 {
            assertionFailure("Marking position that was marked another round.")
        }

        let valIndex = value - 1
        stats.solution[position] = value

        let possInd = getPossibilityIndex(valueIndex: valIndex, cell: position)
        if stats.possibilities[possInd] != 0 {
            assertionFailure("Marking impossible position.")
        }

        // Take this value out of the possibilities for everything in the row
        stats.solutionRound[position] = round
        let rowStart = cellToRow(cell: position) * kColumnRowSectionSize
        for col in 0..<kColumnRowSectionSize {
            let rowVal = rowStart + col
            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: rowVal)
            if stats.possibilities[valPos] == 0 {
                stats.possibilities[valPos] = round
            }
        }

        // Take this value out of the possibilities for everything in the column
        let colStart = cellToColumn(cell: position)
        for i in 0..<kColumnRowSectionSize {
            let colVal = colStart + (kColumnRowSectionSize * i)
            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: colVal)
            if stats.possibilities[valPos] == 0 {
                stats.possibilities[valPos] = round
            }
        }

        // Take this value out of the possibilities for everything in section
        let secStart = cellToSectionStartCell(cell: position)
        for i in 0..<kGridSize {
            for j in 0..<kGridSize {
                let secVal = secStart + i + (kColumnRowSectionSize * j)
                let valPos = getPossibilityIndex(valueIndex: valIndex, cell: secVal)
                if stats.possibilities[valPos] == 0 {
                    stats.possibilities[valPos] = round
                }
            }
        }

        // This position itself is determined, it should have possibilities.
        for valIndex in 0..<kColumnRowSectionSize {
            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
            if stats.possibilities[valPos] == 0 {
                stats.possibilities[valPos] = round
            }
        }
    }

    func findPositionWithFewestPossibilities() -> Int {
        var minPossibilities = 10
        var bestPosition = 0
        for i in 0..<kBoardSize {
            let position = stats.randomBoardArray[i]
            if stats.solution[position] == 0 {
                var count = 0
                for valIndex in 0..<kColumnRowSectionSize {
                    let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
                    if stats.possibilities[valPos] == 0 {
                        count += 1
                    }
                }
                if count < minPossibilities {
                    minPossibilities = count
                    bestPosition = position
                }
            }
        }
        return bestPosition
    }

    func handleNakedPairs(round: Int) -> Bool {
        for position in 0..<kBoardSize {
            let possibilities = countPossibilities(position: position)
            if possibilities == 2 {
                let row = cellToRow(cell: position)
                let column = cellToColumn(cell: position)
                let section = cellToSectionStartCell(cell: position)
                for position2 in position..<kBoardSize {
                    if position != position2 {
                        let possibilities2 = countPossibilities(position: position2)
                        if possibilities2 == 2, arePossibilitiesSame(position1: position, position2: position2) {
                            if row == cellToRow(cell: position2) {
                                var doneSomething = false
                                for column2 in 0..<kColumnRowSectionSize {
                                    let position3 = columnRowToCell(column: column2, row: row)
                                    if position3 != position,
                                       position3 != position2,
                                       removePossibilitiesInOneFromTwo(
                                           position1: position,
                                           position2: position3,
                                           round: round
                                       ) {
                                        doneSomething = true
                                    }
                                }
                                if doneSomething {
                                    if stats.logHistory || stats.recordHistory {
                                        addHistoryItem(logItem: LogItem(round: round, type: .nakedPairRow, value: 0, position: position))
                                    }
                                    return true
                                }
                            }
                            if column == cellToColumn(cell: position2) {
                                var doneSomething = false
                                for row2 in 0..<kColumnRowSectionSize {
                                    let position3 = columnRowToCell(column: column, row: row2)
                                    if position3 != position,
                                       position3 != position2,
                                       removePossibilitiesInOneFromTwo(
                                           position1: position,
                                           position2: position3,
                                           round: round
                                       ) {
                                        doneSomething = true
                                    }
                                }
                                if doneSomething {
                                    if stats.logHistory || stats.recordHistory {
                                        addHistoryItem(logItem: LogItem(round: round, type: .nakedPairColumn, value: 0, position: position))
                                    }
                                    return true
                                }
                            }
                            if section == cellToSectionStartCell(cell: position2) {
                                var doneSomething = false
                                let secStart = cellToSectionStartCell(cell: position)
                                for i in 0..<kGridSize {
                                    for j in 0..<kGridSize {
                                        let position3 = secStart + i + (kColumnRowSectionSize * j)
                                        if position3 != position,
                                           position3 != position2,
                                           removePossibilitiesInOneFromTwo(
                                               position1: position,
                                               position2: position3,
                                               round: round
                                           ) {
                                            doneSomething = true
                                        }
                                    }
                                }
                                if doneSomething {
                                    if stats.logHistory || stats.recordHistory {
                                        addHistoryItem(logItem: LogItem(round: round, type: .nakedPairSection, value: 0, position: position))
                                    }
                                    return true
                                }
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    func countPossibilities(position: Int) -> Int {
        var count = 0
        for valIndex in 0..<kColumnRowSectionSize {
            let valPos = getPossibilityIndex(valueIndex: valIndex, cell: position)
            if stats.possibilities[valPos] == 0 {
                count += 1
            }
        }
        return count
    }

    func arePossibilitiesSame(position1: Int, position2: Int) -> Bool {
        for valIndex in 0..<kColumnRowSectionSize {
            let valPos1 = getPossibilityIndex(valueIndex: valIndex, cell: position1)
            let valPos2 = getPossibilityIndex(valueIndex: valIndex, cell: position2)
            if stats.possibilities[valPos1] == 0 || stats.possibilities[valPos2] == 0, stats.possibilities[valPos1] != 0 || stats.possibilities[valPos2] != 0 {
                return false
            }
        }
        return true
    }

    func addHistoryItem(logItem: LogItem) {
        if stats.logHistory {
            _ = logItem.print()
        }
        if stats.recordHistory {
            stats.solveHistory.append(logItem)
            stats.solveInstructions.append(logItem)
        }
    }

    func shuffleRandomArrays() {
        shuffleArray(array: &stats.randomBoardArray, size: kBoardSize)
        shuffleArray(array: &stats.randomPossibilityArray, size: kColumnRowSectionSize)
    }

    func printBoard(sudoku: [Int]) -> String {
        var output = ""
        for i in 0..<kBoardSize {
            if stats.printStyle == .readable {
                output += " "
            }

            if sudoku[i] == 0 {
                output += "."
            } else {
                output += String(sudoku[i])
            }

            if i == kBoardSize - 1 {
                if stats.printStyle == .csv {
                    output += ","
                } else {
                    //                    output += "\n"
                }
                if stats.printStyle == .readable || stats.printStyle == .compact {
                    output += "\n"
                }
            } else if i % kColumnRowSectionSize == kColumnRowSectionSize - 1 {
                if stats.printStyle == .readable || stats.printStyle == .compact {
                    output += "\n"
                }

                if i % kSectionGroupSize == kSectionGroupSize - 1 {
                    if stats.printStyle == .readable {
                        output += "------|------|------" + "\n"
                    }
                }
            } else if i % kGridSize == kGridSize - 1 {
                if stats.printStyle == .readable {
                    output += "|"
                }
            }
        }

        print(output)
        return output
    }

    func rollbackNonGuesses() {
        // Guesses are odd rounds
        // Non-guesses are even rounds
        for i in 2..<stats.lastSolveRound {
            rollbackRound(round: i)
        }
    }

    func clearPuzzle() {
        // Clear any existing puzzle
        for i in 0..<kBoardSize {
            stats.puzzle[i] = 0
        }
        _ = reset()
    }

    func printHistory(v: [LogItem]) -> String {
        var output = ""
        if stats.recordHistory == false {
            output += "History was not recorded"
            if stats.printStyle == .csv {
                output += " -- "
            } else {
                output += "\n"
            }
        }

        for i in 0..<v.count {
            output += String(i + 1) + ". "
            output += v[i].print()
            if stats.printStyle == .csv {
                output += " -- "
            } else {
                output += "\n"
            }
        }
        if stats.printStyle == .csv {
            output += ","
        } else {
            output += "\n"
        }
        print(output)
        return output
    }

    func removePossibilitiesInOneFromTwo(position1: Int, position2: Int, round: Int) -> Bool {
        var doneSomething = false
        for valIndex in 0..<kColumnRowSectionSize {
            let valPos1 = getPossibilityIndex(valueIndex: valIndex, cell: position1)
            let valPos2 = getPossibilityIndex(valueIndex: valIndex, cell: position2)
            if stats.possibilities[valPos1] == 0, stats.possibilities[valPos2] == 0 {
                stats.possibilities[valPos2] = round
                doneSomething = true
            }
        }
        return doneSomething
    }
}

public enum HighlightStyle: UInt, Identifiable, Codable, CaseIterable, CustomStringConvertible {
    public var id: UInt { rawValue }

    case activeDigit = 0b00000001 // 1 << 0
    case activeDigitColumns = 0b00000010 // 1 << 1
    case activeDigitRows = 0b00000100 // 1 << 2
    case activeDigitSubgrids = 0b00001000 // 1 << 3

    public var description: String {
        switch self {
        case .activeDigit: return "Digit"
        case .activeDigitColumns: return "Columns"
        case .activeDigitRows: return "Rows"
        case .activeDigitSubgrids: return "Sub-Grids"
        }
    }
}
