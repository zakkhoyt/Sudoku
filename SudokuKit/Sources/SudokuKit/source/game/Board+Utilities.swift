//
//  Board+Utilities.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Board {
    #warning("FIXME: @zakkhoyt x, y -> col, row")
    #warning("FIXME: @zakkhoyt index -> cellIndex, or all to index. Consistency. the only 'index' should be cell as row, col, subgrid imply an index")

    func cellAt(x: Int, y: Int) -> Cell {
        cells[indexFor(x: x, y: y)]
    }

    // ---

    func indexFor(x: Int, y: Int) -> Int {
        y * 9 + x
    }

    func columnRow(index: Int) -> (x: Int, y: Int) {
        (columnFor(index: index), rowFor(index: index))
    }

    // ---

    func rowFor(index: Int) -> Int {
        index / 9
    }

    func columnFor(index: Int) -> Int {
        index % 9
    }

    func subgrid(cellIndex: Int) -> Int {
        let (x, y) = columnRow(index: cellIndex)
        return subgrid(x: x, y: y)
    }

    // ---

    func rowFor(x: Int, y: Int) -> Int { y }

    func columnFor(x: Int, y: Int) -> Int { x }

    func subgrid(x: Int, y: Int) -> Int {
        let subgridX = x / 3
        let subgridY = y / 3
        return subgridY * 3 + subgridX
    }

    // ---

    // swiftlint:disable large_tuple

    func columnRowSubgrid(
        index: Int
    ) -> (row: Int, column: Int, subgrid: Int) {
        let (x, y) = columnRow(index: index)
        return columnRowSubgrid(x: x, y: y)
    }

    /// Returns a tuple (row, column, subgrid)
    func columnRowSubgrid(
        x: Int, y: Int
    ) -> (row: Int, column: Int, subgrid: Int) {
        let subgrid = y * 3 + x
        return (y, x, subgrid)
    }

    /// Return all indexes for the row of `index`
    func rowIndexes(index: Int) -> [Int] {
        let y = columnRow(index: index).y
        return (0..<9).map { y * 9 + $0 }
    }

    /// Return all indexes for the column of `index`
    func columnIndexes(index: Int) -> [Int] {
        let x = columnRow(index: index).x
        return (0..<9).map { x + 9 * $0 }
    }

    // swiftlint:enable large_tuple
    func subgridIndexes(cellIndex: Int) -> [Int] {
        let subgrid = subgrid(cellIndex: cellIndex)
        let startX = (subgrid % 3) * 3
        let startY = (subgrid / 3) * 3
        let endX = startX + 3
        let endY = startY + 3

        var indexes: [Int] = []
        for x in startX..<endX {
            for y in startY..<endY {
                indexes.append(indexFor(x: x, y: y))
            }
        }
        return indexes
    }

    // swiftlint:enable large_tuple
    func indexesFor(subgrid: Int) -> [(x: Int, y: Int)] {
        let startX = (subgrid % 3) * 3
        let startY = (subgrid / 3) * 3
        let endX = startX + 3
        let endY = startY + 3

        var indexes: [(x: Int, y: Int)] = []
        for x in startX..<endX {
            for y in startY..<endY {
                indexes.append((x, y))
            }
        }
        return indexes
    }

    func generate(difficulty: Difficulty) -> Generator? {
        // while true {
        for i in 0..<100 {
            let generator = Generator()
            generator.setRecordHistory(recordHistory: true)
            generator.setLogHistory(logHistory: true)
            generator.setPrintStyle(printStyle: .oneLine)
            generator.generatePuzzleSymmetry(inSymmetry: .random)
            if generator.solve() {
                print("\(i)Difficulty after solved: \(generator.difficulty.description)")
                if generator.difficulty == difficulty {
                    return generator
                }
            }
        }
        return nil
    }
}
