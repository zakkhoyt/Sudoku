//
//  Board+Plays.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Board {
    func isPlayValid(value: Int, x: Int, y: Int) -> Bool {
        let section = subgrid(x: x, y: y)

        return
            valueValidForSection(section: section, value: value) &&
            valueValidForRow(row: y, value: value) &&
            valueValidForColumn(col: x, value: value)
    }

    func valueValidForSection(section: Int, value: Int) -> Bool {
        var startX = section % 3
        startX *= 3
        let endX = startX + 2

        var startY = section / 3
        startY *= 3
        let endY = startY + 2

        let set = NSMutableSet()
        for x in startX...endX {
            for y in startY...endY {
                let cell = cellAt(x: x, y: y)
                if let playedValue = cell.correctDigit {
                    set.add(playedValue)
                }
            }
        }

        return !set.contains(value)
    }

    func valueValidForRow(row: Int, value: Int) -> Bool {
        let start = row * 9
        let end = start + 9
        let set = NSMutableSet()
        for i in start..<end {
            let cell = cells[i]
            if let playedValue = cell.correctDigit {
                set.add(playedValue)
            }
        }

        return !set.contains(value)
    }

    func valueValidForColumn(col: Int, value: Int) -> Bool {
//        let start = col
        let set = NSMutableSet()
//        for i in start.stride(to: 9*9, by: 9) {
//        for i in start.stride(from:0, to: 9*9, by: 9) {
        for l in Board.numberRange {
            let i = col + l * 9
            let cell = cells[i]
            if let playedValue = cell.correctDigit {
                set.add(playedValue)
            }
        }

        return !set.contains(value)
    }

    func removeFromNoteValues(value: Int) {
        for cell in cells {
            if let index = cell.noteDigits.firstIndex(where: { $0 == value }) {
                cell.noteDigits.remove(at: index)
            }
        }
    }

    func removeNoteAfterPlayAt(x: Int, y: Int) {
        let cell = cellAt(x: x, y: y)
        // Get answer in cell
        if let answer = cell.solution {
            // Remove that value in the row
            for column in Board.numberRange {
                let cellToEdit = cellAt(x: column, y: y)
                if let indexToRemove = cellToEdit.noteDigits.firstIndex(where: { $0 == answer }) {
                    cellToEdit.noteDigits.remove(at: indexToRemove)
                }
            }

            // Remove that value in the column
            for row in Board.numberRange {
                let cellToEdit = cellAt(x: x, y: row)
                if let indexToRemove = cellToEdit.noteDigits.firstIndex(where: { $0 == answer }) {
                    cellToEdit.noteDigits.remove(at: indexToRemove)
                }
            }

            // Remove that value in the section
            let section = subgrid(x: x, y: y)
            let indexes = indexesFor(subgrid: section)
            for index in indexes {
                let cellToEdit = cellAt(x: index.x, y: index.y)
                // if let indexToRemove = cellToEdit.noteValues.indexOf(answer) {
                if let indexToRemove = cellToEdit.noteDigits.firstIndex(where: { $0 == answer }) {
                    cellToEdit.noteDigits.remove(at: indexToRemove)
                }
            }
        }
    }
}
