//
//  Cell.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

protocol SudokuDrawable: Identifiable {
    var boardIndex: Int { get }
    var normalizedRect: CGRect { get }
}

public class Cell: SudokuDrawable, ObservableObject {
    public enum DigitStatus: Int {
        case unplayed
        case cannotPlay
        case incorrect
        case correct
    }

    public enum NoteStatus: Int {
        case unnoted
        case cannotNote
        case noteAdded
        case noteRemoved
    }
    // MARK: Implements SudokuDrawable

    public var id: Int { boardIndex }
    public let boardIndex: Int
    public let normalizedRect: CGRect

    // MARK: vars
    
    /// What to display for the cell when in digit mode
    public internal(set)  var digitStatus: DigitStatus = .unplayed
    
    /// What to display for the cell when in note mode
    public internal(set)  var noteStatus: NoteStatus = .unnoted

    /// The solution. The answer. The thing the player needs to guess.
    public internal(set) var solution: Int?

    /// This is what the user has guessed.
    public internal(set) var correctDigit: Int?

    /// A set of note digits that the user has entered.
    public internal(set)  var noteDigits: Set<Int> = []

    /// A list of incorrect digits that the player has tried
    public internal(set)  var incorrectDigits: Set<Int> = []

    public init(
        boardIndex: Int,
        normalizedRect: CGRect
    ) {
        self.boardIndex = boardIndex
        self.normalizedRect = normalizedRect
    }

    public var description: String {
        if let playedValue = correctDigit {
            return "[\(playedValue)]"
        } else {
            return "[ ]"
        }
    }
}

public class SubgridCell: SudokuDrawable {
    // MARK: Implements SudokuDrawable

    public var id: Int { boardIndex }
    public let boardIndex: Int
    public let normalizedRect: CGRect

    #warning("FIXME: @zakkhoyt - rename these to hintRect, decorativeHintRect")
    public var strikeoutRect: CGRect {
        normalizedRect
    }
    
    public var decorativeStrikeoutRect: CGRect {
        normalizedRect.insetBy(
            dx: 0.33 / 2 * normalizedRect.size.width,
            dy: 0.33 / 2 * normalizedRect.size.height
        )
    }

    // MARK: inits

    public init(
        boardIndex: Int,
        normalizedRect: CGRect
    ) {
        self.boardIndex = boardIndex
        self.normalizedRect = normalizedRect
    }
}

// TODO: SubgridCellm, RowCell, ColumnCell are dupes
public class RowCell: SudokuDrawable {
    // MARK: Implements SudokuDrawable

    public var id: Int { boardIndex }
    public let boardIndex: Int
    public let normalizedRect: CGRect

    public var strikeoutRect: CGRect {
        normalizedRect
    }
    public var decorativeStrikeoutRect: CGRect {
        normalizedRect.insetBy(dx: 0, dy: normalizedRect.size.height * 0.45)
    }

    // MARK: inits

    public init(
        boardIndex: Int,
        normalizedRect: CGRect
    ) {
        self.boardIndex = boardIndex
        self.normalizedRect = normalizedRect
    }
}

public class ColumnCell: SudokuDrawable {
    // MARK: Implements SudokuDrawable

    public var id: Int { boardIndex }
    public let boardIndex: Int
    public let normalizedRect: CGRect

    public var strikeoutRect: CGRect {
        normalizedRect
    }
    
    public var decorativeStrikeoutRect: CGRect {
        normalizedRect.insetBy(dx: normalizedRect.size.width * 0.45, dy: 0)
    }

    // MARK: inits

    public init(
        boardIndex: Int,
        normalizedRect: CGRect
    ) {
        self.boardIndex = boardIndex
        self.normalizedRect = normalizedRect
    }
}
