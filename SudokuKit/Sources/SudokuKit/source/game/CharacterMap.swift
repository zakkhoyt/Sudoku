//
//  CharacterMap.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 6/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

public class CharacterMap {
    public static let standard = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    public static let standardCircled = ["⓵", "⓶", "⓷", "⓸", "⓹", "⓺", "⓻", "⓼", "⓽"]
    public static let emojiRandom1 = ["❤️", "👍🏼", "💛", "💙", "💚", "😡", "🐞", "🌓", "🏉", "⚾️", "🎹"]
    public static let maps = [
        standard,
        standardCircled,
        emojiRandom1
    ]

    public static func string(characterSet: [String]) -> String {
        characterSet.joined()
    }

    // public var characters: [String] = ["⓵", "⓶", "⓷", "⓸", "⓹", "⓺", "⓻", "⓼", "⓽"]
    public var characters: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

    public init() {}

    public init(characters: [String]) {
        self.characters = characters
    }

    public func character(index: Int) -> String {
        if index > 9 {
            return "!"
        }
        return characters[index]
    }
}
