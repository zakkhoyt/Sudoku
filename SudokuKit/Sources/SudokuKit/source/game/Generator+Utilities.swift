//
//  Generator+Utilities.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/5/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//
//  SudokuKit.Generator is a Swift 3.0 port of QQWing availble here: https://github.com/stephenostermiller/qqwing
//  Copyright (C) 2016 Zakk Hoyt (vaporwarewolf@gmail.com)
//  Copyright (C) 2006-2014 Stephen Ostermiller http://ostermiller.org/
//  Copyright (C) 2007 Jacques Bensimon (jacques@ipm.com)
//  Copyright (C) 2011 Jean Guillerez (j.guillerez - orange.fr)
//  Copyright (C) 2014 Michael Catanzaro (mcatanzaro@gnome.org)
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

import Foundation

func shuffleArray(array: inout [Int], size: Int) {
    for i in 0..<size {
        let tailSize = size - 1
        let r = Int.random(in: 0..<size)
        let randTailPos = r % tailSize
        let temp = array[i]
        array[i] = array[randTailPos]
        array[randTailPos] = temp
    }
}

func getRandomSymmetry() -> Symmetry {
    switch Int.random(in: 0..<4) {
    case 0:
        return .rotate90
    case 1:
        return .rotate180
    case 2:
        return .mirror
    case 3:
        return .flip
    default:
        return .rotate90
    }
}

func getLogCount(v: [LogItem], type: LogType) -> Int {
    var count = 0
    for i in 0..<v.count {
        if v[i].getLogType() == type {
            count += 1
        }
    }
    return count
}

func cellToColumn(cell: Int) -> Int {
    cell % kColumnRowSectionSize
}

func cellToRow(cell: Int) -> Int {
    cell / kColumnRowSectionSize
}

func cellToSection(cell: Int) -> Int {
    (cell / kSectionGroupSize * kGridSize)
        + (cellToColumn(cell: cell) / kGridSize)
}

func cellToSectionStartCell(cell: Int) -> Int {
    (cell / kSectionGroupSize * kSectionGroupSize)
        + (cellToColumn(cell: cell) / kGridSize * kGridSize)
}

func rowToFirstCell(row: Int) -> Int {
    kColumnRowSectionSize * row
}

func columnToFirstCell(column: Int) -> Int {
    column
}

func sectionToFirstCell(section: Int) -> Int {
    (section % kGridSize * kGridSize)
        + (section / kGridSize * kSectionGroupSize)
}

func getPossibilityIndex(valueIndex: Int, cell: Int) -> Int {
    valueIndex + (kColumnRowSectionSize * cell)
}

func columnRowToCell(column: Int, row: Int) -> Int {
    (row * kColumnRowSectionSize) + column
}

func sectionToCell(section: Int, offset: Int) -> Int {
    sectionToFirstCell(section: section)
        + ((offset / kGridSize) * kColumnRowSectionSize)
        + (offset % kGridSize)
}
