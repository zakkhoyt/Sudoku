//
//  PuzzleReader.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 6/27/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

public struct Puzzle: Codable {
    let difficulty: Difficulty
    let difficultyString: String
    let puzzleString: String
    let solutionString: String
}

public class PuzzleGenerator {
    public enum Error: Swift.Error {
        case invalidPregeneratedFileURL
        case failedToGeneratePuzzles
    }

    /// WIll generate `count` `Puzzle` instances
    public static func generate(count: Int) -> [Puzzle] {
        (0..<count).compactMap { i in
            guard let generator = Generator.generateRandomSymmetryAndSolve() else {
                return nil
            }

            logger.debug("generated \(i + 1) / \(count) puzzles")

            return Puzzle(
                difficulty: generator.difficulty,
                difficultyString: generator.difficulty.description,
                puzzleString: generator.stats.puzzle.map { "\($0)" }.joined(),
                solutionString: generator.stats.solution.map { "\($0)" }.joined()
            )
        }
    }

    /// WIll regenerate `count` puzzles then overrwirte the local data store.
    @discardableResult
    public static func regenerate(count: Int) throws -> URL {
        do {
            let url = try pregeneratedFileUrl()

            let puzzles = generate(count: count)
            guard !puzzles.isEmpty else {
                throw Error.failedToGeneratePuzzles
            }

            let encoder = JSONEncoder()
            encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
            let data = try encoder.encode(puzzles)
            guard let jsonString = String(data: data, encoding: .utf8) else {
                logger.error("failed to convert data to string")
                throw NSError()
            }
            logger.debug("\n\n\(jsonString)\n\n")

            try data.write(to: url)
            return url
        } catch {
            logger.error("Failed to regenerate new puzzles for local data store: \(error, privacy: .public)")
            throw error
        }
    }

    /// Reads local stores for puzzles that match `difficulty`. Picks one at random to return.
    public static func randomPregenerated(
        difficulty: Difficulty
    ) throws -> Board? {
        do {
            logger.debug("Will search pregenerated puzzles for difficulty: \(difficulty.description, privacy: .public)")
            let url = try pregeneratedFileUrl()
            let data = try Data(contentsOf: url)
            let puzzles = try JSONDecoder().decode([Puzzle].self, from: data)
            let matchingPuzzles: [Puzzle] = puzzles.filter { $0.difficulty == difficulty }
            guard !matchingPuzzles.isEmpty else {
                logger.error("Failed to find any pregenerated puzzles of difficulty: \(difficulty, privacy: .public)")
                assertionFailure("Failed to find any pregenerated puzzles of difficulty: \(difficulty)")
                return nil
            }
            logger.debug("Did find \(matchingPuzzles.count) pregenerated puzzles for difficulty: \(difficulty, privacy: .public)")
            let i = Int.random(in: 0...matchingPuzzles.count - 1)
            logger.debug("Using random index of \(i)")
            let puzzle = matchingPuzzles[i]
            let board = Board(puzzle: puzzle)
            return board
        } catch {
            logger.error("Failed to find a pregenerated puzzle for difficulty: \(difficulty, privacy: .public) with error:\n\(error)")
            throw error
        }
    }

    private static func pregeneratedFileUrl() throws -> URL {
        guard let url = Bundle.module.url(
            forResource: "pregenerated", withExtension: "json"
        ) else {
            logger.error("Failed to get URL refrence to puzzles.csv")
            throw Error.invalidPregeneratedFileURL
        }
        return url
    }
}

extension Generator {
    fileprivate static func generateRandomSymmetryAndSolve() -> Generator? {
        let generator = Generator()
        generator.setRecordHistory(recordHistory: true)
        generator.setLogHistory(logHistory: true)
        generator.setPrintStyle(printStyle: .oneLine)
        generator.generatePuzzleSymmetry(inSymmetry: .random)
        guard generator.solve() else {
            logger.error("Failed to solve puzzle with symmetry: .random")
            return nil
        }
        return generator
    }
}
