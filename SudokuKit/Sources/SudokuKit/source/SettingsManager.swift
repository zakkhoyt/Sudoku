//
//  Magic.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 6/28/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Combine
import Foundation
import SwiftUI

public class SettingsManager: ObservableObject {
    public static let shared = SettingsManager()

    public enum Error: Swift.Error {
        case appGroupFailure
    }

    private let userDefaults: UserDefaults
    private let settingsKey = "settings"

    public let settingsPublisher = CurrentValueSubject<Settings, Never>(Settings())

    public init() {
        self.userDefaults = UserDefaults(suiteName: kAppGroup) ?? .standard
        self.settings = read()
    }

    @Published public var settings = Settings() {
        didSet {
            write()
        }
    }

    public func write() {
        do {
            let data = try JSONEncoder().encode(settings)
            userDefaults.set(data, forKey: settingsKey)
//            print("Wrote settings. settings.highlightAlpha: \(settings.highlightAlpha)")
        } catch {
            assertionFailure(error.localizedDescription)
        }
    }

    public func delete() {
        userDefaults.removeObject(forKey: settingsKey)
    }

    private func read() -> Settings {
        guard let data = userDefaults.object(
            forKey: settingsKey
        ) as? Data else {
            print("Could not read settings from userDefaults")
            return Settings()
        }

        do {
            let settings = try JSONDecoder().decode(Settings.self, from: data)
            print("Did read settings from userDefaults")
            return settings
        } catch let error as DecodingError {
            print("error: \(error.debugDescription)")
            delete()
            assertionFailure("error: \(error.debugDescription)")
            return Settings()
        } catch {
            print("error: \(error)")
            delete()
            assertionFailure("error: \(error)")
            return Settings()
        }
    }
}

public struct Settings: Codable, Hashable {
    public enum StartingCellsStyle: Int, Codable, CaseIterable, CustomStringConvertible {
        case qqwing
        case depopulate

        public var description: String {
            switch self {
            case .qqwing: return "Type Q"
            case .depopulate: return "Type D"
            }
        }
    }

    public var startingCellsStyle = StartingCellsStyle.depopulate
    public var characterSetIndex = 0
    public var showMistakes = true
    public var difficulty: Difficulty = .easy
    public var highlightSelectedNumberWhereCorrect = true
    public var highlightStyles: Set<HighlightStyle> = [.activeDigit] {
        didSet {
            print("highlightStyle: \(highlightStyles)")
        }
    }

    public var hintShadingStyle: HintShadingStyle = .monoColor
    public var hintBlendMode: HintBlendMode = .normal
    public var hintAlpha: Double = 1

    public init() {}

    // FIXME: Custom Decoder that can gracefully decode

    public mutating func toggleHighlightStyle(_ highlightStyle: HighlightStyle) {
        if highlightStyles.contains(highlightStyle) {
            highlightStyles.remove(highlightStyle)
        } else {
            highlightStyles.insert(highlightStyle)
        }
    }
}

extension Settings {
    public enum HintShadingStyle: String, CaseIterable, Codable, CustomStringConvertible, Identifiable {
        case monoColor
        case decorative
        case none
        
        public var id: String { rawValue }
        public var title: String {
            switch self {
            // .color (but is very dark)
            // (full alpha) .normal, .lighten
            // .exclusion is kind of fun at full alpha
            case .monoColor: "Single Color"
            case .decorative: "Decorative"
            case .none: "None"
            }
        }
        
        public var description: String {
            switch self {
            case .monoColor: "\(rawValue.capitalized) - Shades each row, column, and subgrid the same color."
            case .decorative: "\(rawValue.capitalized) - Decorates each row, column, and subgrid with colors and geometry."
            case .none: "\(rawValue.capitalized) - No hint styling."
            }
        }
    }
    
    public enum HintBlendMode: String, CaseIterable, Codable, CustomStringConvertible, Identifiable {
        public var id: String { rawValue }

        case normal
        case multiply
        case screen
        case overlay
        case darken
        case lighten
        case colorDodge
        case colorBurn
        case softLight
        case hardLight
        case difference
        case exclusion
        case hue
        case saturation
        case color
        case luminosity
        case sourceAtop
        case destinationOver
        case destinationOut
        case plusDarker
        case plusLighter

        public var blendMode: SwiftUI.BlendMode {
            switch self {
            case .normal: return .normal
            case .multiply: return .multiply
            case .screen: return .screen
            case .overlay: return .overlay
            case .darken: return .darken
            case .lighten: return .lighten
            case .colorDodge: return .colorDodge
            case .colorBurn: return .colorBurn
            case .softLight: return .softLight
            case .hardLight: return .hardLight
            case .difference: return .difference
            case .exclusion: return .exclusion
            case .hue: return .hue
            case .saturation: return .saturation
            case .color: return .color
            case .luminosity: return .luminosity
            case .sourceAtop: return .sourceAtop
            case .destinationOver: return .destinationOver
            case .destinationOut: return .destinationOut
            case .plusDarker: return .plusDarker
            case .plusLighter: return .plusLighter
            }
        }

        public var title: String {
            switch self {
            case .normal: return "Normal"
            case .multiply: return "Multiply"
            case .screen: return "Screen"
            case .overlay: return "Overlay"
            case .darken: return "Darken"
            case .lighten: return "Lighten"
            case .colorDodge: return "Color Dodge"
            case .colorBurn: return "Color Burn"
            case .softLight: return "Soft Light"
            case .hardLight: return "Hard Light"
            case .difference: return "Difference"
            case .exclusion: return "Exclusion"
            case .hue: return "Hue"
            case .saturation: return "Saturation"
            case .color: return "Color"
            case .luminosity: return "Luminosity"
            case .sourceAtop: return "Source Atop"
            case .destinationOver: return "Destination Over"
            case .destinationOut: return "Destination Out"
            case .plusDarker: return "Plus Darker"
            case .plusLighter: return "Plus Lighter"
            }
        }
        
        public var description: String { title }

        public init(
            blendMode: SwiftUI.BlendMode
        ) {
            switch blendMode {
            case .normal: self = .normal
            case .multiply: self = .multiply
            case .screen: self = .screen
            case .overlay: self = .overlay
            case .darken: self = .darken
            case .lighten: self = .lighten
            case .colorDodge: self = .colorDodge
            case .colorBurn: self = .colorBurn
            case .softLight: self = .softLight
            case .hardLight: self = .hardLight
            case .difference: self = .difference
            case .exclusion: self = .exclusion
            case .hue: self = .hue
            case .saturation: self = .saturation
            case .color: self = .color
            case .luminosity: self = .luminosity
            case .sourceAtop: self = .sourceAtop
            case .destinationOver: self = .destinationOver
            case .destinationOut: self = .destinationOut
            case .plusDarker: self = .plusDarker
            case .plusLighter: self = .plusLighter
            @unknown default: self = .normal
            }
        }
    }
}
