//
//  Utilities.swift
//  SudokuKit
//
//  Created by Zakk Hoyt on 7/1/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

let kAppGroup = "group.com.vaporwarewolf.Sudoku"

// TODO: Move this to SudokuApp
public enum Utilities {
    public static var documentsDirURL: URL {
        guard let dir = NSSearchPathForDirectoriesInDomains(
            .documentDirectory,
            .userDomainMask,
            true
        ).first else {
            preconditionFailure("Failed to get documents directory")
        }
        let url = URL(fileURLWithPath: dir)
        return url
    }

    public static var appGroupDirURL: URL {
        guard let appGroupDirUrl = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: kAppGroup) else {
            preconditionFailure("Failed to get app group directory")
        }
        return appGroupDirUrl
    }
}
