//
//  Appearance.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/23/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import SwiftUI
import SudokuKit
import UIKit

struct ColorShader {
    enum ShaderSource {
        case cell
        case column
        case row
        case subgrid
    }

    static func color(
        source: ShaderSource,
        style: Settings.HintShadingStyle,
        opacity o: Double
    ) -> SwiftUI.Color {
        // TODO: Can we move this struct to `settingsManager.setting`? or move that data here. Where does it belong?
        // TODO: Can we remove the opacity param? If not can we remove @autoclosure?

        switch style {
        case .monoColor: return .green.opacity(o)
        case .decorative:
            let color: SwiftUI.Color = {
                switch source {
                case .cell: .green
                case .column: .green
                case .row: .red
                case .subgrid: .blue
                }
            }()
            return color.opacity(o)
        case .none:
            return .green
        }
    }
}

extension UILabel {
    dynamic var defaultFont: UIFont? {
        get { font }
        set { font = newValue }
    }
}

extension UIColor {
    static var playColor: UIColor { .systemGreen }
    static var notesColor: UIColor { .systemYellow }
    static var gameBackgroundColor: UIColor { .systemBackground }
    static var tint: UIColor { playColor }
    static var secondaryTintColor: UIColor { notesColor }
    static var majorGridColor: UIColor { .label }
    static var minorGridColor: UIColor { .secondaryLabel }
    static var errorColor: UIColor { .systemRed }
    static var highlightBackgroundColor: UIColor { tertiaryLabel.withAlphaComponent(0.5) }
    static var notesBackgroundColor: UIColor { notesColor.withAlphaComponent(0.25) }
    static var completeBackgroundColor: UIColor { playColor.withAlphaComponent(0.25) }
    static var incompleteBackgroundColor: UIColor { .clear }
}

class Appearance {
    class func setup() {
        do {
            let loadedFonts = try FontLoader.loadFonts()
            print("Did load fonts:\n\(loadedFonts.joined(separator: ", "))")
        } catch {
            print(error.localizedDescription)
        }
        
        let barFont = UIFont.arcadeFont(size: 30)
        let barAttr = [NSAttributedString.Key.foregroundColor: UIColor.tint, NSAttributedString.Key.font: barFont]
        UINavigationBar.appearance().titleTextAttributes = barAttr
        UINavigationBar.appearance().barTintColor = UIColor.darkGray
        UINavigationBar.appearance().tintColor = UIColor.tint
        
        let barbFont = UIFont.arcadeFont()
        let barbAttr = [NSAttributedString.Key.foregroundColor: UIColor.tint, NSAttributedString.Key.font: barbFont]
        UIBarButtonItem.appearance().setTitleTextAttributes(barbAttr, for: .normal)
        
        UIToolbar.appearance().barTintColor = .gameBackgroundColor
        UIToolbar.appearance().tintColor = .tint
        
        UITableViewCell.appearance().backgroundColor = .gameBackgroundColor
        UITableView.appearance().backgroundColor = .gameBackgroundColor
        
        UIStepper.appearance().tintColor = .tint
        
        UIButton.appearance().tintColor = .tint
        
        UILabel.appearance().textColor = .majorGridColor
        UILabel.appearance().font = .arcadeFont()
        
        UILabel.appearance(whenContainedInInstancesOf: [UIButton.self]).textColor = .tint
        UILabel.appearance(whenContainedInInstancesOf: [UIButton.self]).font = UIFont.arcadeFont()
        
        UICollectionView.appearance().backgroundColor = .darkGray
        
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).textColor = .tint
        UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).font = UIFont.arcadeFont()
    }
    
    class func defaultFont(size: CGFloat) -> UIFont {
        UIFont.arcadeFont(size: size)
    }
    
    class func title() -> String {
        Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String ?? "Sudoku"
    }
}

extension UIFont {
    static func arcadeFont(size: CGFloat = 20) -> UIFont {
        let kArcadeClassicFontName = "ArcadeClassic"
        guard let font = UIFont(name: kArcadeClassicFontName, size: size) else {
            preconditionFailure("Failed to load \(kArcadeClassicFontName) font")
        }
        return font
    }
}

extension UIViewController {
    func setNavigationBarToClearBackground() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    func setNavigationBarToDefaultAppearance() {
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.isTranslucent = false
    }
}
