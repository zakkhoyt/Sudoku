//
//  AudioController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/10/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import AVFoundation

enum AudioFile: String, CaseIterable {
    case action00
    case collect00
    case collect01
    case collect02
    case fail00
    case fail01
    case goal00
    case goal01
    case goal02
    case goal03
    case goal04
    case goal05
    case nav00
    case nav01
    case nav02
    case nav03
    case numbers00
    case pickup00
    case pickup01
    case pickup02
    case shoot00
}

class AudioController {
    struct AudioFileProperties {
        
    }
    
    static let sharedInstance = AudioController()
    private var audio = [String: AVAudioPlayer]()
    private let queue = DispatchQueue.global(qos: .userInitiated)
    
    private let audioEngine = AVAudioEngine()

    
    private init() {
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(
                .playback,
                mode: .default,
                options: [
                    .mixWithOthers, // .duckOthers might be good
                    .allowBluetoothA2DP,
                    .allowAirPlay
                ]
            )
        } catch {
            logger.error("[ERROR] audioSession.setCategory(...) failed")
        }
        
        do {
            try audioSession.setActive(true)
        } catch {
            logger.error("[ERROR] audioSession.setActive(true) failed")
        }
        
        preloadAudioEffects(effectFileNames: AudioFile.allCases.map { $0.rawValue })
    }
    
    private func preloadAudioEffects(effectFileNames: [String]) {
        guard let bundle = Bundle.AudioSamplesBundle() else {
            assertionFailure("no AudioSamplesBundle")
            return
        }

        // TODO: This is a terrible way to do this (making a player for each sound ahead of time!!
        // This is noticably slow on older iOS devices as well.
        effectFileNames.forEach {
            guard let soundURL = bundle.url(forResource: $0, withExtension: "wav") else {
                assertionFailure("Failed to get url for sound file " + $0)
                return
            }

//            do {
//                let player = try AVAudioPlayer(contentsOf: soundURL)
//                player.numberOfLoops = 0
//                player.currentTime = 0
//                player.prepareToPlay()
//                audio[$0] = player
//                logger.debug("Did load audio sample: \($0)")
//            } catch {
//                logger.error("Failed to load audio sample: \($0)")
//                assertionFailure("Load sound failed " + $0)
//            }
            
            // MARK - new
            
//            /* An AVAudioFile instance that points to file that's open for reading. */
//            let audioFile = AVAudioFile(forReading: soundURL)
//
//            let playerNode = AVAudioPlayerNode()
//
//            // Attach the player node to the audio engine.
//            audioEngine.attach(playerNode)
//            
//            // Connect the player node to the output node.
//            audioEngine.connect(
//                playerNode,
//                to: audioEngine.outputNode,
//                format: audioFile.processingFormat
//            )
        }
        
        
    }
    
    func playEffect(audioFile: AudioFile) {
        let date = Date()
        print("queueing playing \(audioFile.rawValue)")
//        queue.async { [weak self] in
//            guard let self else { return }
//            let fileName = audioFile.rawValue
//            guard let player = self.audio[fileName] else {
//                logger.error("Failed to find player for audio sample: \(fileName, privacy: .public)")
//                assertionFailure("no player for audio sample: \(fileName)")
//                return
//            }
//
//            let elapsed0 = Date().timeIntervalSince(date)
//            print("will play \(audioFile.rawValue) \(elapsed0)")
//            if player.isPlaying {
//                player.currentTime = 0
//            } else {
//                player.play()
//            }
//            let elapsed1 = Date().timeIntervalSince(date)
//            print("did play \(audioFile.rawValue) \(elapsed1)")
//            logger.debug("Did play audio sample: \(fileName, privacy: .public)")
//        }
        
        // MARK - new
        
//r
        
    }
}
