//
//  PopupMenuViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/11/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

class PopupMenuViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var buttonTitles: [String] = [] {
        didSet {
            reloadMenu()
        }
    }

    var selectHandler: ((_ index: Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gameBackgroundColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func reloadMenu() {
//        tableView.beginUpdates()
//        tableView.endUpdates()
        guard let tableView else {
            return
        }
        tableView.reloadData()
        tableViewHeightConstraint.constant = tableView.contentSize.height
    }
}

extension PopupMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection numberOfRowsInsection: Int) -> Int {
        buttonTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTableViewCell") as? MenuItemTableViewCell else {
            preconditionFailure("Failed to dequeue MenuItemTableViewCell")
        }
        cell.menuItemLabel.text = buttonTitles[indexPath.row]
        return cell
    }
}

extension PopupMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        64
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        selectHandler?(indexPath.row)
        dismiss(animated: false, completion: nil)
    }
}
