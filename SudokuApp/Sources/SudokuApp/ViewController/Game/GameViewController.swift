//
//  GameViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Combine
import LocalAuthentication
import SudokuKit
import UIKit

class GameViewController: BaseViewController {
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var playsLabel: UILabel!
    @IBOutlet weak var validPlaysLabel: UILabel!
    @IBOutlet weak var invalidPlaysLabel: UILabel!
    
    @IBOutlet weak var activeNumberLabel: UILabel!
    @IBOutlet weak var timeElapsedLabel: UILabel!
    @IBOutlet weak var squaresLeftLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var notesSwitch: UISwitch!
    @IBOutlet weak var quitBarb: UIBarButtonItem!
    @IBOutlet weak var bottomStackView: UIStackView!
    private var numbersView = NumbersView(numberRange: Board.numberRange)

//    private lazy var settingsManager = SettingsManager()
    private lazy var settings: Settings = SettingsManager.shared.settings
//    private lazy var settings: Settings = settingsManager.settingsPublisher.value
//    private var subscriptions = Set<AnyCancellable>()
    
    private lazy var board: Board = {
        let difficulty = settings.difficulty
        
        let board: Board = {
            // Look for a pregenerated puzzle (much faster). Else generate one
            if let board = try? PuzzleGenerator.randomPregenerated(
                difficulty: difficulty
            ) {
                print("Using pregenrated puzzle")
                return board
            } else {
                print("Failed to find a pregenrated puzzle of difficulty: \(difficulty.description). Generating a new one.")
                return Board(difficulty: difficulty)
            }
        }()
        
        board.highlightStyles = settings.highlightStyles
        return board
    }()
    
    private var dynamicFontSize: CGFloat {
        (collectionView.contentSize.width / 9) * 0.5
    }
    
    private var characterMap = CharacterMap()
    private var audio = AudioController.sharedInstance
    private var initialLayout = false
    private var majorGridViews: [UIView] = []
    private var minorGridViews: [UIView] = []
    private var observers: [Any] = []
    
    override var prefersStatusBarHidden: Bool { true }
    
    deinit {
        observers.forEach {
            NotificationCenter.default.removeObserver($0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        resetGame()
        updateUI()
        setupNotificationObservers()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.presentNewGameScreen()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        drawMinorGrid()
        drawMajorGrid()
        updateUI()
    }

    private func setupNotificationObservers() {
        observers.append(
            NotificationCenter
                .default
                .addObserver(
                    forName: UserDefaults.didChangeNotification,
                    object: nil,
                    queue: OperationQueue.main
                ) { _ in
                    self.resetUI()
                }
        )
        
//        settingsManager.settingsPublisher
//            .sink { [weak self] settings in
//                self?.settings = settings
//            }
//            .store(in: &subscriptions)
    }
    
    private func setupUI() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        difficultyLabel.font = UIFont.arcadeFont(size: 30)
        playsLabel.font = UIFont.arcadeFont(size: 20)
        validPlaysLabel.font = UIFont.arcadeFont(size: 20)
        invalidPlaysLabel.font = UIFont.arcadeFont(size: 20)
        
        bottomStackView.insertArrangedSubview(numbersView, at: 0)
        numbersView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        numbersView.buttonTapped = { [weak self] i in
            self?.board.activeNumberIndex = i
            self?.audio.playEffect(audioFile: .nav03)
            self?.updateUI()
        }
        
        collectionView.backgroundColor = .clear
        collectionView.backgroundView?.backgroundColor = .clear
    }

    private func resetGame() {
        board.indexesToHightlightUpdated = { [weak self] in
            guard let self else { return }
            self.collectionView.reloadData()
            self.updateUI()
        }
        
        board.calculateIndexesToHighlight()
        
        resetUI()
    }
    
    private func drawMajorGrid() {
        // remove existing
        majorGridViews.forEach { $0.removeFromSuperview() }

        // Major grid lines
        let thirdHeight = collectionView.contentSize.height / 3
        let thirdWidth = collectionView.contentSize.width / 3
        let width = collectionView.contentSize.width
        let height = collectionView.contentSize.height
        let lineWidth = CGFloat(5.0)
        
        (0...3).forEach { x in
            let frame = CGRect(
                origin: CGPoint(
                    x: CGFloat(x) * thirdWidth - lineWidth / 2,
                    y: 0
                ),
                size: CGSize(
                    width: lineWidth,
                    height: height
                )
            )
            let lineView = UIView(frame: frame)
            lineView.backgroundColor = .majorGridColor
            collectionView.addSubview(lineView)
            majorGridViews.append(lineView)
        }
        
        (0...3).forEach { y in
            let frame = CGRect(
                origin: CGPoint(
                    x: 0,
                    y: CGFloat(y) * thirdHeight - lineWidth / 2
                ),
                size: CGSize(
                    width: width,
                    height: lineWidth
                )
            )
            let lineView = UIView(frame: frame)
            lineView.backgroundColor = .majorGridColor
            collectionView.addSubview(lineView)
            majorGridViews.append(lineView)
        }
    }
    
    private func drawMinorGrid() {
        // remove existing
        minorGridViews.forEach { $0.removeFromSuperview() }
        
        // Major grid lines
        let ninthHeight = collectionView.contentSize.height / 9.0
        let ninthWidth = collectionView.contentSize.width / 9.0
        let width = collectionView.contentSize.width
        let height = collectionView.contentSize.height
        let lineWidth = CGFloat(1.0)
        
        for x in Board.numberRange {
            let frame = CGRect(
                origin: CGPoint(
                    x: CGFloat(x) * ninthWidth - lineWidth / 2,
                    y: 0
                ),
                size: CGSize(
                    width: lineWidth,
                    height: height
                )
            )
            let lineView = UIView(frame: frame)
            lineView.backgroundColor = .minorGridColor
            collectionView.addSubview(lineView)
            minorGridViews.append(lineView)
        }
        
        for y in Board.numberRange {
            let frame = CGRect(
                origin: CGPoint(
                    x: 0,
                    y: CGFloat(y) * ninthHeight - lineWidth / 2
                ),
                size: CGSize(
                    width: width,
                    height: lineWidth
                )
            )
            let lineView = UIView(frame: frame)
            lineView.backgroundColor = .minorGridColor
            collectionView.addSubview(lineView)
            minorGridViews.append(lineView)
        }
    }
    
    private func resetUI() {
        // Characters
        characterMap.characters = CharacterMap.maps[settings.characterSetIndex]
        
        // Score
        updateHeaderLabels()
        
        // Notes
        notesSwitch.isOn = false
        
        // Reload
        collectionView.reloadData()
    }
    
    private func updateUI() {
        updateNumberView(
            viewModel: NumberViewModel(
                mode: notesSwitch.isOn ? .notes : .play,
                states: Board.numberRange.map { [weak self] i in
                    guard let self else { return .incomplete }
                    if self.board.activeNumberIndex == i {
                        return .active
                    } else if self.board.isDigitComplete(digit: i + 1) {
                        return .complete
                    } else {
                        return .incomplete
                    }
                },
                fontSize: max(dynamicFontSize, 20)
            )
        )
        updateHeaderLabels()
    }
    
    private func updateNumberView(viewModel: NumberViewModel) {
        viewModel.states.enumerated().forEach { [weak self] iterator in
            guard let self else { return }
            let button = self.numbersView.buttons[iterator.offset]
            button.titleLabel?.font = UIFont.arcadeFont(size: viewModel.fontSize)
            button.alpha = iterator.element.alpha
            button.isUserInteractionEnabled = iterator.element.isUserInteractionEnabled
            switch viewModel.mode {
            case .play:
                button.backgroundColor = iterator.element.playBackgroundColor
                button.setTitleColor(iterator.element.playFontColor, for: .normal)
            case .notes:
                button.backgroundColor = iterator.element.notesBackgroundColor
                button.setTitleColor(iterator.element.noteFontColor, for: .normal)
            }
        }
    }
    
    private func alert(message: String) {
        let ac = UIAlertController(
            title: nil,
            message: message,
            preferredStyle: .alert
        )
        ac.addAction(
            UIAlertAction(
                title: "Okay",
                style: .default
            ) { _ in
                self.dismiss(animated: true)
            }
        )
        present(ac, animated: true)
    }
    
    private func updateHeaderLabels() {
        difficultyLabel.text = board.difficulty.description
        playsLabel.text = "Moves: \(board.playCounter)"
        validPlaysLabel.text = "Correct: \(board.validCounter)"
        invalidPlaysLabel.text = "Error: \(board.invalidCounter)"
        
        activeNumberLabel.text = "Digit: \(board.activeNumberIndex + 1)"
        timeElapsedLabel.text = "--:--"
        
        squaresLeftLabel.text = "Remain: \(board.unplayedCellCount)"
    }
    
    private func flashBackground(color: UIColor) {
        view.backgroundColor = color
        UIView.animate(withDuration: 1.0) {
            self.view.backgroundColor = .gameBackgroundColor
        }
    }
    
    private func showEmitter(frame: CGRect) {
        func makeEmitterCell(color: UIColor) -> CAEmitterCell {
            let cell = CAEmitterCell()
            cell.birthRate = 20
            cell.lifetime = 2.0
            cell.lifetimeRange = 1.5
            cell.color = color.cgColor
            cell.velocity = 200
            cell.velocityRange = 100
            cell.emissionLongitude = CGFloat.pi
            cell.emissionRange = CGFloat.pi * 2
            cell.spin = 2
            cell.spinRange = 3
            cell.scaleRange = 0.5
            cell.scaleSpeed = -0.05
            cell.contents = UIImage(named: "confetti")?.cgImage
            return cell
        }

        let particleEmitter = CAEmitterLayer()
        particleEmitter.emitterPosition = frame.centerPoint
        particleEmitter.emitterShape = .rectangle
        particleEmitter.emitterSize = CGSize(width: frame.width, height: 1)
        particleEmitter.emitterCells = [
            makeEmitterCell(color: .red),
            makeEmitterCell(color: .green),
            makeEmitterCell(color: .blue)
        ]
        particleEmitter.beginTime = CACurrentMediaTime()
        view.layer.addSublayer(particleEmitter)
                
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { _ in
            particleEmitter.birthRate = 0
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { _ in
                particleEmitter.removeFromSuperlayer()
            }
        }
    }
    
    private func presentNewGameScreen() {
        let navigationController = UINavigationController(
            rootViewController: GameUIViewController(
                settingsManager: SettingsManager.shared,
                board: board
            )
        )
//        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true)
    }
        
    @IBAction
    private func quitBarbAction(_ sender: AnyObject) {
        let ac = UIAlertController(
            title: "Quit?",
            message: "All game progress will be lost.",
            preferredStyle: .alert
        )
        ac.addAction(
            UIAlertAction(
                title: "New UI",
                style: .default
            ) { [weak self] _ in
                guard let self else { return }
                self.presentNewGameScreen()
            }
        )

        ac.addAction(
            UIAlertAction(
                title: "Yes",
                style: .destructive
            ) { [weak self] _ in
                self?.dismiss(animated: false, completion: nil)
            }
        )
        ac.addAction(
            UIAlertAction(
                title: "No",
                style: .cancel
            )
        )
        present(ac, animated: true, completion: nil)
    }
    
    @IBAction
    private func notesSwitchAction(_ sender: UISwitch) {
        board.actionMode = sender.isOn ? .notes : .digit
        collectionView.reloadData()
        audio.playEffect(audioFile: .pickup02)
        updateUI()
    }
}

extension GameViewController: UICollectionViewDataSource {
    func numberOfSections(
        in collectionView: UICollectionView
    ) -> Int {
        settings = SettingsManager.shared.settings
        // Get a fresh copy now so we don't re-query for every collectionViewCell.
        return 1
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        81
    }
  
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "SudokuCollectionViewCell",
            for: indexPath
        ) as? SudokuCollectionViewCell else {
            preconditionFailure("Failed to dequeue SudokuCollectionViewCell")
        }
        
        // Scale font size to screen
        cell.valueLabel.font = UIFont.arcadeFont(size: dynamicFontSize)
        cell.notesLabel.font = UIFont.arcadeFont(size: dynamicFontSize * 0.5)
        
        let boardCell = board.cells[indexPath.item]
        
        // Render the notes label and background color
        cell.setNotes(notes: Array(boardCell.noteDigits).sorted())
        
        // Render the played number and background color
        if let value = boardCell.correctDigit {
            let valueString = String(value)
            
            cell.valueLabel.textColor = .tint
            cell.valueLabel.text = valueString
            cell.valueLabel.text = characterMap.character(index: value - 1)
            
            if boardCell.solution == board.activeNumberIndex + 1,
               settings.highlightStyles.contains(.activeDigit) {
                // Highlighting active number on
                cell.backgroundColor = .highlightBackgroundColor
            } else {
                // Highlighting active number off
                cell.backgroundColor = .completeBackgroundColor
            }
        } else if !boardCell.noteDigits.isEmpty {
            // note color
            cell.backgroundColor = .notesBackgroundColor
        } else {
            // incomplete
            cell.backgroundColor = .incompleteBackgroundColor
        }
        
        if board.cellIndexesToHightlight.contains(indexPath.item) {
            cell.highlightView.backgroundColor = .highlightBackgroundColor.withAlphaComponent(0.2)
        } else {
            cell.highlightView.backgroundColor = .clear
        }
        
        return cell
    }
}

extension GameViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let s = collectionView.bounds.size.width / 9.0
        return CGSize(width: s, height: s)
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        0
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        0
    }
}

extension GameViewController: UICollectionViewDelegate {
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        collectionView.deselectItem(at: indexPath, animated: false)
        let digit = board.activeNumberIndex + 1
        let cell = board.play(
            digit: digit,
            at: indexPath.item
        ) 
        switch cell.digitStatus {
        case .cannotPlay:
            break
        case .incorrect:
            if settings.showMistakes == true {
                if let cell = collectionView.cellForItem(
                    at: indexPath
                ) as? SudokuCollectionViewCell {
                    cell.valueLabel.backgroundColor = .errorColor
                }
            }
            flashBackground(color: .errorColor)
            audio.playEffect(audioFile: .shoot00)
        case .correct:
            
            // Update cell
            if let cell = collectionView.cellForItem(at: indexPath) as? SudokuCollectionViewCell {
                //                cell.valueLabel.text = map.character(v)
                cell.valueLabel.textColor = .tint
                cell.valueLabel.backgroundColor = .clear
            }
            
            // Update numbersSegment
            if board.isDigitComplete(digit: digit) {
                if let activeNumberIndex = nextIncompleteNumberIndex() {
                    board.activeNumberIndex = activeNumberIndex
                }
                
                flashBackground(color: .tint)
                
                if let cell = collectionView.cellForItem(at: indexPath) {
                    let frame = collectionView.convert(cell.frame, to: nil)
                    print("emitter frame: \(frame)")
                    showEmitter(frame: frame)
                }
                
                audio.playEffect(audioFile: .goal05)
            } else {
                audio.playEffect(audioFile: .goal02)
            }
            
            // Check if finshed
            if board.isBoardSolved() {
                alert(message: "You Win!!")
            }
            
            collectionView.reloadData()
        case .unplayed:
            break
        }
        
        switch cell.noteStatus {
        case .unnoted:
            break
        case .cannotNote:
            break
        case .noteAdded:
            collectionView.reloadData()
            audio.playEffect(audioFile: .collect00)
        case .noteRemoved:
            collectionView.reloadData()
            audio.playEffect(audioFile: .collect00)
        }
        
        updateUI()
    }
    
    private func nextIncompleteNumberIndex() -> Int? {
        let delta = Board.numberRange.upperBound - Board.numberRange.lowerBound
        return (0..<delta)
            .map { (board.activeNumberIndex + $0) % delta }
            .first { !board.isDigitComplete(digit: $0 + 1) }
    }
}

extension CGRect {
    var centerPoint: CGPoint {
        CGPoint(
            x: origin.x + size.width / 2,
            y: origin.y + size.height / 2
        )
    }
}
