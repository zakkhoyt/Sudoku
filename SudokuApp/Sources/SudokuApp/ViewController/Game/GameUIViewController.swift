//
//  GameUIViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 5/21/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI
import UIKit

class GameUIViewController: UIViewController {
    private let settingsManager: SettingsManager
    private let board: Board
    
    private lazy var gameScreen = GameScreen(
        board: board,
        settingsManager: settingsManager
    ) { [weak self] _ in
        self?.dismiss(animated: false, completion: nil)
    }
    
    init(
        settingsManager: SettingsManager,
        board: Board
    ) {
        self.settingsManager = settingsManager
        self.board = board
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        embedSwiftUI(view: gameScreen)
    }
}
