//
//  BaseTableViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/10/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import UIKit

class BaseTableViewController: UITableViewController {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let closeBarb = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(closeBarbAction))
        navigationItem.leftBarButtonItem = closeBarb
    }
    
    override var prefersStatusBarHidden: Bool { true }
    
    @objc
    func closeBarbAction(_ sender: UIBarButtonItem) {
        viewControllerWillDismiss()
        dismiss(animated: false, completion: nil)
        AudioController.sharedInstance.playEffect(audioFile: .nav02)
        viewControllerDidDismiss()
    }
    
    func viewControllerWillDismiss() {
        Console.warning("Child must implement")
    }
    
    func viewControllerDidDismiss() {
        Console.warning("Child must implement")
    }
}
