//
//  AboutViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/10/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.font = Appearance.defaultFont(size: 40)
        titleLabel.text = Appearance.title()
        
        infoLabel.text = [
            "Written by Zakk Hoyt",
            "Copywright 2022",
            Bundle.main.versionAndBuildString
        ]
            .compactMap { $0 }
            .joined(separator: "\n")
    }
    
    // MARK: overrides

    override func viewControllerWillDismiss() {
        print("do stuff here")
    }
}

extension Bundle {
    public var appName: String? {
        guard let value = object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String else {
            return nil
        }
        return value
    }
    
    // This will return nil if called from Framework unit tests
    public var versionString: String? {
        guard let value = object(forInfoDictionaryKey: "CFBundleShortVersionString" as String) as? String else {
            return nil
        }
        return value
    }
    
    public var buildString: String? {
        guard let value = object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String else {
            return nil
        }
        return value
    }
    
    public var versionAndBuildString: String? {
        guard let versionString,
              let buildString else {
            return nil
        }
        return "v\(versionString) b\(buildString)"
    }
}
