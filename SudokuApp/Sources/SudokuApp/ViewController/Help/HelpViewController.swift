//
//  HelpViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/10/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

class HelpViewController: BaseViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gameBackgroundColor
    }
}
