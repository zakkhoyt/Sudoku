//
//  MenuViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/10/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI
import UIKit

class MenuViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gameBackgroundColor
    }

    // MARK: IBActions

    @IBAction
    func backButtonTouchUpInside(_ sender: AnyObject) {}
    
    @IBAction
    func playButtonTouchUpInside(_ sender: AnyObject) {
        let board: Board = {
            let difficulty = SettingsManager.shared.settings.difficulty
            
            let board: Board = {
                // Look for a pregenerated puzzle (much faster). Else generate one
                if let board = try? PuzzleGenerator.randomPregenerated(
                    difficulty: difficulty
                ) {
                    print("Using pregenrated puzzle")
                    return board
                } else {
                    print("Failed to find a pregenrated puzzle of difficulty: \(difficulty.description). Generating a new one.")
                    return Board(difficulty: difficulty)
                }
            }()
            
            board.highlightStyles = SettingsManager.shared.settings.highlightStyles
            return board
        }()
        
        let viewController = GameUIViewController(
            settingsManager: SettingsManager.shared,
            board: board
        )
        let navigationController = UINavigationController(rootViewController: viewController)
        AudioController.sharedInstance.playEffect(audioFile: .nav00)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        present(navigationController, animated: true)
    }

    @IBAction
    func settingsButtonTouchUpInside(_ sender: AnyObject) {
        present(
            UIHostingController(
                rootView: SettingsView(
                    settingsManager: SettingsManager.shared
                ) { [weak self] index in
                    self?.presentedViewController?.dismiss(animated: true)
                }
            ),
            animated: true
        )
    }
    
    @IBAction
    func helpButtonTouchUpInside(_ sender: AnyObject) {
        present(storyBoardNamed: "Help")
    }

    @IBAction
    func aboutButtonTouchUpInside(_ sender: AnyObject) {
        present(storyBoardNamed: "About")
    }
    
    // MARK: Helpers

    private func present(storyBoardNamed: String) {
        let storyboard = UIStoryboard(
            name: storyBoardNamed,
            bundle: .module
        )
        
        guard let viewController = storyboard.instantiateInitialViewController() else {
            return
        }
        
        let navigationController = UINavigationController(rootViewController: viewController)
        AudioController.sharedInstance.playEffect(audioFile: .nav00)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        present(navigationController, animated: true)
    }
}
