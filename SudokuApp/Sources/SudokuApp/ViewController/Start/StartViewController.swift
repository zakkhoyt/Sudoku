//
//  StartViewController.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/10/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    @IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .gameBackgroundColor
        
        titleLabel.font = Appearance.defaultFont(size: 40)
        titleLabel.text = Appearance.title()
    }

//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }
}
