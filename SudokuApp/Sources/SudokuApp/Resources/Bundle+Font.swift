//
//  Bundle+Font.swift
//  HatchTools
//
//  Created by Zakk Hoyt on 1/25/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import CoreText
import Foundation

public enum FontLoader {
    public enum Error: Swift.Error {
        case noFontBundle
    }

    /// Loads all fonts in HatchAssets/Resources/Fonts/Fonts.bundle
    /// - Returns: A list of font names that were loaded
    /// - Throws: Any error encountered during font loading
    @discardableResult
    public static func loadFonts() throws -> [String] {
        guard let fontBundle = Bundle.module.fontBundle else {
            throw Error.noFontBundle
        }
        return try fontBundle.loadFonts()
    }
}

public enum FontError: Error {
    case failedToLocateFile
    case failedToCreateProvider
    case failedToCreateFont
    case failedToRegisterFont(CGFont)
    case failed
}

extension Bundle {
    /// Returns the Bunle which represents `Fonts.bundle`
    fileprivate var fontBundle: Bundle? {
        guard let url = url(
            forResource: "Fonts",
            withExtension: "bundle"
        ) else {
            return nil
        }
        return Bundle(url: url)
    }
    
    /// Loads all fonts in HatchAssets/Resources/Fonts/Fonts.bundle
    /// - Returns: A list of font names that were loaded
    /// - Throws: Any error encountered during font loading
    @discardableResult
    public func loadFonts() throws -> [String] {
        try FileManager
            .default
            .contentsOfDirectory(atPath: bundlePath)
            .filter { $0.contains(".ttf") || $0.contains(".otf") }
            .sorted()
            .compactMap {
                do {
                    let parts = $0.split(separator: ".")
                    guard parts.count == 2 else { return nil }
                    return try loadFont(
                        name: String(parts[0]),
                        type: String(parts[1])
                    )
                } catch {
                    throw error
                }
            }
    }
    
    /// Loads font programmatically using CoreText and C pointers
    /// - Parameters:
    ///   - name: The name of the font file. E.g.: "SFUIText-Regular"
    ///   - type: The extension of the font file. E.g.: "ttf"
    private func loadFont(name: String, type: String) throws -> String {
        guard let path = path(forResource: name, ofType: type) else {
            throw FontError.failedToLocateFile
        }
        
        let url = URL(fileURLWithPath: path)
        let data = try Data(contentsOf: url) as CFData
        guard let provider = CGDataProvider(data: data) else {
            throw FontError.failedToCreateProvider
        }
        guard let font = CGFont(provider) else {
            throw FontError.failedToCreateFont
        }
        
        var error: Unmanaged<CFError>?
        guard CTFontManagerRegisterGraphicsFont(font, &error) else {
            if let error = error as? Error {
                throw error
            } else {
                throw FontError.failedToRegisterFont(font)
            }
        }
        return name
    }
}
