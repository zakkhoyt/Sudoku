//
//  Board+DigitsViewModel+State.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/22/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import Foundation
import SudokuKit
import UIKit

enum DigitsViewTheme {
    static let completedSegmentAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.arcadeFont(),
        .foregroundColor: UIColor.blue,
        .backgroundColor: UIColor.clear
    ]
    
    static let enabledSegmentAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.arcadeFont(),
        .foregroundColor: UIColor.blue,
        .backgroundColor: UIColor.clear
    ]
    
    static let disabledSegmentAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.arcadeFont(),
        .foregroundColor: UIColor.blue,
        .backgroundColor: UIColor.clear
    ]
}

extension Board.DigitsViewModel.State {
    var playFontColor: UIColor {
        switch self {
        case .active: return .playColor
        case .incomplete: return .playColor
        case .complete: return .playColor
        }
    }

    var noteFontColor: UIColor {
        switch self {
        case .active: return .notesColor
        case .incomplete: return .notesColor
        case .complete: return .notesColor
        }
    }

    var alpha: CGFloat {
        switch self {
        case .active: return 1
        case .incomplete: return 1
        case .complete: return 0.25
        }
    }
    
    var playBackgroundColor: UIColor {
        switch self {
        case .active: return .highlightBackgroundColor
        case .incomplete: return .clear
        case .complete: return .clear
        }
    }
    
    var notesBackgroundColor: UIColor {
        switch self {
        case .active: return .notesBackgroundColor
        case .incomplete: return .clear
        case .complete: return .clear
        }
    }
    
    var isUserInteractionEnabled: Bool {
        switch self {
        case .active: return true
        case .incomplete: return true
        case .complete: return false
        }
    }
}
