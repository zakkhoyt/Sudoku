//
//  MenuItemTableViewCell.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/11/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {
    @IBOutlet weak var menuItemLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
