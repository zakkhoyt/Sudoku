//
//  NumberViewModel.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 8/10/21.
//  Copyright © 2021 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import UIKit

struct NumberViewTheme {
    static let completedSegmentAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.arcadeFont(),
        .foregroundColor: UIColor.blue,
        .backgroundColor: UIColor.clear
    ]
    
    static let enabledSegmentAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.arcadeFont(),
        .foregroundColor: UIColor.blue,
        .backgroundColor: UIColor.clear
    ]
    
    static let disabledSegmentAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.arcadeFont(),
        .foregroundColor: UIColor.blue,
        .backgroundColor: UIColor.clear
    ]
}

struct NumberViewModel {
    enum NumberMode {
        case play
        case notes
    }

    enum State {
        // The numer is incomplete, and is actively selected
        case active
        
        // The numer is incomplete, but not active
        case incomplete
        
        // The number is complete.
        case complete
    }
    
    /// The mode for all numbers
    let mode: NumberMode
    
    /// One state for each number
    let states: [State]
    
    /// Font size for the number view
    let fontSize: CGFloat
}

extension NumberViewModel.State {
    var playFontColor: UIColor {
        switch self {
        case .active: return .playColor
        case .incomplete: return .playColor
        case .complete: return .playColor
        }
    }

    var noteFontColor: UIColor {
        switch self {
        case .active: return .notesColor
        case .incomplete: return .notesColor
        case .complete: return .notesColor
        }
    }

    var alpha: CGFloat {
        switch self {
        case .active: return 1
        case .incomplete: return 1
        case .complete: return 0.25
        }
    }
    
    var playBackgroundColor: UIColor {
        switch self {
        case .active: return .highlightBackgroundColor
        case .incomplete: return .clear
        case .complete: return .clear
        }
    }
    
    var notesBackgroundColor: UIColor {
        switch self {
        case .active: return .notesBackgroundColor
        case .incomplete: return .clear
        case .complete: return .clear
        }
    }
    
    var isUserInteractionEnabled: Bool {
        switch self {
        case .active: return true
        case .incomplete: return true
        case .complete: return false
        }
    }
}
