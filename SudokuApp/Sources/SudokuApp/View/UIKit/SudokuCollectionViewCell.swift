//
//  SudokuCollectionViewCell.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

class SudokuCollectionViewCell: UICollectionViewCell {
    var wiggle = CAKeyframeAnimation()
    
    @IBOutlet weak var highlightView: UIView!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        notesLabel.font = Appearance.defaultFont(size: 10)
        notesLabel.textColor = .secondaryTintColor
        prepareForReuse()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        valueLabel.text = ""
        valueLabel.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    func fadeIn() {
        valueLabel.transform = valueLabel.transform.scaledBy(x: 5.0, y: 5.0)
        valueLabel.alpha = 0.0
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                self.valueLabel.transform = CGAffineTransform.identity
                self.valueLabel.alpha = 1.0
            },
            completion: { _ in
            }
        )
    }
    
    func setNotes(notes: [Int]) {
        notesLabel.text = notes
            .map { "\($0)" }
            .joined(separator: " ")
    }
}
