//
//  NumbersView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 8/10/21.
//  Copyright © 2021 Zakk Hoyt. All rights reserved.
//

import UIKit

class NumbersView: UIView {
    var buttonTapped: ((Int) -> Void)?
    
    private(set) lazy var buttons: [UIButton] = numberRange.map { i in
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.arcadeFont(size: 20)
        button.tag = i
        button.setTitleColor(.green, for: .normal)
        button.setTitle("\(i + 1)", for: .normal)
        button.addTarget(self, action: #selector(buttonTouchUpInside), for: .touchUpInside)
        return button
    }
    
    private let numberRange: Range<Int>
    private let stackView = UIStackView()
    private let font = UIFont.arcadeFont()
    
    init(numberRange: Range<Int>) {
        self.numberRange = numberRange
        super.init(frame: .zero)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Use `init(numberRange:)` instead.")
    }
    
    private func commonInit() {
        backgroundColor = .clear
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        buttons.forEach {
            let view = UIView()
            view.addSubview($0)
            view.translatesAutoresizingMaskIntoConstraints = false
            let inset: CGFloat = 4
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: inset),
                $0.topAnchor.constraint(equalTo: view.topAnchor, constant: inset),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: inset),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: inset)
            ])
            stackView.addArrangedSubview($0)
        }
    }
    
    @objc
    private func buttonTouchUpInside(sender: UIButton) {
        print("Tapped")
        buttonTapped?(sender.tag)
    }
}
