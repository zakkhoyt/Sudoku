//
//  File.swift
//  
//
//  Created by Zakk Hoyt on 9/18/23.
//

import SwiftUI
import SudokuKit

struct SettingsView: View {
    @ObservedObject var settingsManager: SettingsManager
    @State var didTapExitButton: IndexTapped = { _ in }
    
    var body: some View {
        VStack {
            HStack(spacing: 0) {
                VStack(spacing: 0) {
                    Text("Difficulty")
                    Picker("Difficulty", selection: $settingsManager.settings.difficulty) {
                        ForEach(Difficulty.allCases) { difficulty in
                            Text(difficulty.title)
                                .tag(difficulty)
                                .font(.custom("ArcadeClassic", size: 20))
                        }
                    }
                    .pickerStyle(.menu)
                    .font(.custom("ArcadeClassic", size: 20))
                }
                
                VStack(spacing: 0) {
                    Text("Hint Style")
                    Picker("Shading Style", selection: $settingsManager.settings.hintShadingStyle) {
                        ForEach(Settings.HintShadingStyle.allCases) {
                            Text($0.title)
                                .tag($0)
                                .font(.custom("ArcadeClassic", size: 20))
                        }
                    }
                    .pickerStyle(.menu)
                    .font(.custom("ArcadeClassic", size: 20))
                }
                
                VStack(spacing: 0) {
                    Text("Hint Style")
                    Picker("Blend Mode", selection: $settingsManager.settings.hintBlendMode) {
                        ForEach(Settings.HintBlendMode.allCases) { blendMode in
                            Text(blendMode.title)
                                .tag(blendMode)
                                .font(.custom("ArcadeClassic", size: 20))
                        }
                    }
                    .pickerStyle(.menu)
                    .font(.custom("ArcadeClassic", size: 20))
                }
            }
            VStack(spacing: 0) {
                Text("Highlight Alpha: \(String(format: "%.2f", settingsManager.settings.hintAlpha))")
                Slider(
                    value: $settingsManager.settings.hintAlpha,
                    in: 0.0...1.0
                ) {
                    Text("Alpha")
                }
            }
            HStack(spacing: 0) {
                ForEach(HighlightStyle.allCases) { hightlightStyle in
                    Button(hightlightStyle.description) {
                        print("Toggle hightlightStyle: \(hightlightStyle.description)")
                        #warning("FIXME: @zakkhoyt this shoudln't be done in a SwiftUI view. Make an object.")
                        settingsManager.settings.toggleHighlightStyle(hightlightStyle)
                        
#warning("FIXME: @zakkhoyt commented out board")
//                        board.highlightStyles = settingsManager.settings.highlightStyles
                    }
                    .foregroundColor(settingsManager.settings.highlightStyles.contains(hightlightStyle) ? .green : .green)
                    .tint(settingsManager.settings.highlightStyles.contains(hightlightStyle) ? .green.opacity(0.25) : .clear)
                    .buttonStyle(.borderedProminent)
                    .frame(maxWidth: .infinity) // HERE 1)
                    .font(.custom("ArcadeClassic", size: 12))
                }
            }
            Toggle(
                "Show Mistakes",
                isOn: $settingsManager.settings.showMistakes
            )
            
            // TODO: population style
//            @IBAction
//            func startingCellsStyleSegmentedControlValueChanged(_ sender: UISegmentedControl) {
//                guard let startingCellsStyle = Settings.StartingCellsStyle(rawValue: sender.selectedSegmentIndex) else {
//                    return
//                }
//                settings.startingCellsStyle = startingCellsStyle
//                updateUI()
//            }

            HStack {
                Button("Quit") {
                        print("quit tapped")
                    didTapExitButton(0)
                }
                .foregroundColor(.green)
                .buttonStyle(.bordered)
                .font(.custom("ArcadeClassic", size: 12))
                .padding(.horizontal)
            }
        }
        .padding()
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            GeometryReader { geometry in
                SettingsView(
                    settingsManager: SettingsManager.shared
                )
            }
            .preferredColorScheme(.light)
            .previewInterfaceOrientation(.portrait)
            
            GeometryReader { geometry in
                SettingsView(
                    settingsManager: SettingsManager.shared
                )
            }
            .preferredColorScheme(.dark)
            .previewInterfaceOrientation(.portrait)
        }
    }
}
