//
//  SudokuBoard.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 5/21/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//
//  Toolbard / menu: https://swiftwithmajid.com/2020/08/05/menus-in-swiftui/

import DrawingUI
import Foundation
import SudokuKit
import SwiftUI

typealias IndexTapped = (_ index: Int) -> Void

struct GameGrid: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    let geometry: GeometryProxy
    
    var body: some View {
        ZStack {
            GeometryReader { geometry in
                ForEach(board.columnCells) {
                    ColumnCellView(
                        board: board,
                        settingsManager: settingsManager,
                        columnCell: $0,
                        geometry: geometry
                    )
                    .disabled(true)
                }
                
                ForEach(board.rowCells) {
                    RowCellView(
                        board: board,
                        settingsManager: settingsManager,
                        rowCell: $0,
                        geometry: geometry
                    )
                    .disabled(true)
                }
                                
                ForEach(board.subgridCells) {
                    SubgridCellView(
                        board: board,
                        settingsManager: settingsManager,
                        subgridCell: $0,
                        geometry: geometry
                    )
                    .disabled(true)
                }
                
                ForEach(board.cells) {
                    CellView(
                        board: board,
                        settingsManager: settingsManager,
                        cell: $0,
                        geometry: geometry
                    )
                }
            }
        }
        .frame(
            width: geometry.centeredSquare.width,
            height: geometry.centeredSquare.height
        )
        .position(
            x: geometry.size.width / 2,
            y: geometry.size.height / 2
        )
    }
}

struct SudokuBoard_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            GameGrid(
                board: .previewBoard,
                settingsManager: SettingsManager.shared,
                geometry: geometry
            )
        }
        .previewLayout(.fixed(width: 800.0, height: 800))
        .previewDevice(/*@START_MENU_TOKEN@*/"iPhone 11"/*@END_MENU_TOKEN@*/)
        .previewInterfaceOrientation(.portraitUpsideDown)
    }
}
