//
//  GameBoard.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 5/21/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//
//  https://developer.apple.com/documentation/swiftui/environment

import SudokuKit
import SwiftUI

struct GameScreen: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    @State var didTapExitButton: IndexTapped = { _ in }
    
    @Environment(\.dynamicTypeSize) var typeSize
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @Environment(\.verticalSizeClass) var verticalSizeClass
    
    var body: some View {
        GeometryReader { geometry in
            Color(.systemBackground)
            VStack {
                Text("Difficulty: \(board.difficulty.description)")
                    .foregroundColor(Color(uiColor: .label))
                    .multilineTextAlignment(.center)
                Text("# Plays: \(board.playCounter)")
                    .foregroundColor(Color(uiColor: .label))
                    .multilineTextAlignment(.center)
                Text("# Correct: \(board.validCounter)")
                    .foregroundColor(Color(uiColor: .label))
                    .multilineTextAlignment(.center)
                Text("# Errors: \(board.invalidCounter)")
                    .foregroundColor(Color(uiColor: .label))
                    .multilineTextAlignment(.center)
                GeometryReader { geometry in
                    GameGrid(
                        board: board,
                        settingsManager: settingsManager,
                        geometry: geometry
                    )
                }
                InGameSettingsView(
                    board: board,
                    settingsManager: settingsManager,
                    didTapExitButton: didTapExitButton
                )
                DigitsView(
                    board: board,
                    geometry: geometry
                ) { digitIndex in
                    guard let activeCellIndex = board.activeCellIndex else {
                        return
                    }
                    
                    let digit = digitIndex + 1
                    let cell = board.play(
                        digit: digit,
                        at: activeCellIndex
                    )
                    switch cell.digitStatus {
                    case .cannotPlay: print("cell.digitStatus: cannotPlay")
                    case .incorrect: print("cell.digitStatus: incorrect")
                    case .correct: print("cell.digitStatus: correct")
                    case .unplayed: print("cell.digitStatus: unplayed")
                    }
                    
                    switch cell.noteStatus {
                    case .unnoted: print("cell.noteStatus: unnoted")
                    case .cannotNote: print("cell.noteStatus: cannotNote")
                    case .noteAdded: print("cell.noteStatus: noteAdded")
                    case .noteRemoved: print("cell.noteStatus: noteRemoved")
                    }
                }
            }
            .frame(
                width: geometry.size.width,
                height: geometry.size.height
            )
        }
        .padding(.horizontal)
    }
}

struct GameScreen_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ForEach(Device.previewDevices) { device in
                GameScreen(
                    board: .previewBoard,
                    settingsManager: SettingsManager.shared
                )
                .preferredColorScheme(device.colorScheme)
                .previewDevice(PreviewDevice(rawValue: device.name))
                .previewInterfaceOrientation(device.orientation)
                .previewLayout(device.previewLayout)
            }
        }
    }
}
