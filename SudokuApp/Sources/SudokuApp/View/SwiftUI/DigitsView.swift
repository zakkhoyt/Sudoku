//
//  DigitsView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/22/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI

struct DigitsView: View {
    @ObservedObject var board: Board
    let geometry: GeometryProxy
    @State var didTapDigit: IndexTapped
    
    var body: some View {
        HStack(spacing: 0) {
            ForEach(0..<9) { i in
                DigitButton(
                    i: i,
                    board: board,
                    geometry: geometry,
                    didTapDigit: didTapDigit
                )
            }
        }
    }
}

struct DigitsView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            DigitsView(
                board: .previewBoard,
                geometry: geometry
            ) { _ in
            }
        }
        .previewInterfaceOrientation(.portrait)
    }
}

struct DigitButton: View {
    let i: Int
    @ObservedObject var board: Board
    let geometry: GeometryProxy
    @State var didTapDigit: IndexTapped
    
    var body: some View {
        Button("\(i + 1)") {
//            print("did tap digitIndex: \(i)")
            switch board.actionMode {
            case .digit:
                board.activeNumberIndex = i
            case .notes:
                ()
            }
            
            didTapDigit(i)
        }
        .buttonStyle(.borderedProminent)
        .tint(board.actionMode == .digit ? .green.opacity(0.25) : .yellow.opacity(0.25))
        // .foregroundColor(board.activeNumberIndex == i ? .green : Color(uiColor: .label))
        .foregroundColor(.green)
        .frame(maxWidth: .infinity, minHeight: 40)
        .padding(0)
        .font(.custom("ArcadeClassic", size: 20))
        .opacity(board.isDigitComplete(digit: i) ? 0.1 : 1)
        .disabled(board.isDigitComplete(digit: i))
    }
}
