//
//  RowCellView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/22/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI

struct RowCellView: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    let rowCell: SudokuKit.RowCell
    let geometry: GeometryProxy
    
    var body: some View {
        // FIXME: @zakkhoyt - Refactor this to make it shorter
        switch settingsManager.settings.hintShadingStyle {
        case .monoColor:
            Path(
                geometry.expand(
                    rect: rowCell.strikeoutRect
                )
            )
            .fill(
                ColorShader.color(
                    source: .row,
                    style: settingsManager.settings.hintShadingStyle,
                    opacity: board.highlights.rowIndexes.contains(rowCell.boardIndex) ? settingsManager.settings.hintAlpha : 0
                )
            )
            .blendMode(settingsManager.settings.hintBlendMode.blendMode)
        case .decorative:
            Path(
                geometry.expand(
                    rect: rowCell.decorativeStrikeoutRect
                )
            )
            .fill(
                ColorShader.color(
                    source: .row,
                    style: settingsManager.settings.hintShadingStyle,
                    opacity: board.highlights.rowIndexes.contains(rowCell.boardIndex) ? settingsManager.settings.hintAlpha : 0
                )
            )
            .blendMode(settingsManager.settings.hintBlendMode.blendMode)
        case .none:
            EmptyView()
        }
        
        Path(
            geometry.expand(
                rect: rowCell.normalizedRect
            )
        )
        .stroke(Color(uiColor: .label), lineWidth: 0.5)
    }
}

struct RowCellView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            RowCellView(
                board: .previewBoard,
                settingsManager: SettingsManager.shared,
                rowCell: Board.previewBoard.rowCells[6],
                geometry: geometry
            )
        }
    }
}
