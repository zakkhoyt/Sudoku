//
//  ColumnCellView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/22/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI

struct ColumnCellView: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    let columnCell: SudokuKit.ColumnCell
    let geometry: GeometryProxy
    
    var body: some View {
        // FIXME: @zakkhoyt - Refactor this to make it shorter
        switch settingsManager.settings.hintShadingStyle {
        case .monoColor:
            Path(
                geometry.expand(
                    rect: columnCell.strikeoutRect
                )
            )
            .fill(
                ColorShader.color(
                    source: .column,
                    style: settingsManager.settings.hintShadingStyle,
                    opacity: board.highlights.columnIndexes.contains(columnCell.boardIndex) ? settingsManager.settings.hintAlpha : 0
                )
            )
            .blendMode(settingsManager.settings.hintBlendMode.blendMode)
        case .decorative:
            Path(
                geometry.expand(
                    rect: columnCell.decorativeStrikeoutRect
                )
            )
            .fill(
                ColorShader.color(
                    source: .column,
                    style: settingsManager.settings.hintShadingStyle,
                    opacity: board.highlights.columnIndexes.contains(columnCell.boardIndex) ? settingsManager.settings.hintAlpha : 0
                )
            )
            .blendMode(settingsManager.settings.hintBlendMode.blendMode)
        case .none:
            EmptyView()
        }
        
        Path(
            geometry.expand(
                rect: columnCell.normalizedRect
            )
        )
        .stroke(Color(uiColor: .label), lineWidth: 0.5)
    }
}

struct ColumnCellView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            ColumnCellView(
                board: .previewBoard,
                settingsManager: SettingsManager.shared,
                columnCell: Board.previewBoard.columnCells[2],
                geometry: geometry
            )
        }
    }
}
