//
//  Board+Preview.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 10/8/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import Foundation
import SudokuKit
import SwiftUI

extension Board {
    // Serves as an instance for all SwiftUI previews
    static let previewBoard: Board = {
        let difficulty = Difficulty.easy
        // Look for a pregenerated puzzle (much faster). Else generate one
        if let board = try? PuzzleGenerator.randomPregenerated(
            difficulty: difficulty
        ) {
            print("Using pregenrated puzzle")
            return board
        } else {
            print("Failed to find a pregenrated puzzle of difficulty: \(difficulty.description). Generating a new one.")
            return Board(difficulty: difficulty)
        }
    }()
}

struct Device: Identifiable {
    let id: UUID
    let name: String
    let previewLayout: PreviewLayout
    let orientation: InterfaceOrientation
    let colorScheme: ColorScheme
    
    init(
        id: UUID = UUID(),
        name: String,
        previewLayout: PreviewLayout = .device,
        orientation: InterfaceOrientation = .portrait,
        colorScheme: ColorScheme = .light
    ) {
        self.id = id
        self.name = name
        self.previewLayout = previewLayout
        self.orientation = orientation
        self.colorScheme = colorScheme
    }
    
    static let previewDevices: [Device] = [
        Device(name: "1", previewLayout: .device, orientation: .portrait, colorScheme: .light),
        Device(name: "2", previewLayout: .device, orientation: .portrait, colorScheme: .dark)
//        Device(name: "3", previewLayout: .device, orientation: .landscapeLeft, colorScheme: .light),
//        Device(name: "4", previewLayout: .device, orientation: .landscapeRight, colorScheme: .dark),
//        Device(name: "5", previewLayout: .sizeThatFits, orientation: .portrait, colorScheme: .light),
//        Device(name: "6", previewLayout: .fixed(width: 800, height: 800), orientation: .portrait, colorScheme: .light)
    ]
}
