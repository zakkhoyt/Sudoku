//
//  SubgridCellView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/22/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI

struct SubgridCellView: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    let subgridCell: SudokuKit.SubgridCell
    let geometry: GeometryProxy
    
    var body: some View {
        ZStack {
            // FIXME: @zakkhoyt - Refactor this to make it shorter
            switch settingsManager.settings.hintShadingStyle {
            case .monoColor:
                Path(
                    geometry.expand(
                        rect: subgridCell.strikeoutRect
                    )
                )
                 // TODO: Consider a different option. stroking a strikeout line
                .fill(
                    ColorShader.color(
                        source: .subgrid,
                        style: settingsManager.settings.hintShadingStyle,
                        opacity: board.highlights.subgridIndexes.contains(subgridCell.boardIndex) ? settingsManager.settings.hintAlpha : 0
                    )
                )
                .blendMode(settingsManager.settings.hintBlendMode.blendMode)
            case .decorative:
                Path(
                    geometry.expand(
                        rect: subgridCell.decorativeStrikeoutRect
                    )
                )
                .stroke(
                    ColorShader.color(
                        source: .subgrid,
                        style: settingsManager.settings.hintShadingStyle,
                        opacity: board.highlights.subgridIndexes.contains(subgridCell.boardIndex) ? settingsManager.settings.hintAlpha : 0
                    ),
                    // TODO: Instead of hardcoding line width, update SubgridCell.strikeoutRect (maybe rename to strikeoutPath?)
                    style: StrokeStyle(
                        lineWidth: 8,
                        lineCap: .round,
                        dash: [0, 16]
                    )
                )
                .blendMode(settingsManager.settings.hintBlendMode.blendMode)
            case .none:
                EmptyView()
            }

            
            Path(
                geometry.expand(
                    rect: subgridCell.normalizedRect
                )
            )
            .stroke(Color(uiColor: .label), lineWidth: 3)
        }
    }
}

struct SubgridCellView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            SubgridCellView(
                board: .previewBoard,
                settingsManager: SettingsManager.shared,
                subgridCell: Board.previewBoard.subgridCells[4],
                geometry: geometry
            )
        }
    }
}
