//
//  CellView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/22/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import DrawingUI
import Foundation
import SudokuKit
import SwiftUI

struct CellView: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    @ObservedObject var cell: SudokuKit.Cell
    let geometry: GeometryProxy
    
    var body: some View {
        ZStack {
            // Background color, captures tap event
            Rectangle()
                .stroke(Color(uiColor: .label))
                .contentShape(Rectangle()) // required for.`onTapGesture`.
                .onTapGesture {
                    print("did tap cellIndex: \(cell.boardIndex)")
                    board.activeCellIndex = cell.boardIndex
                }
            
            
            if let answer = cell.correctDigit {
                // Cell's text content (played digits)
                Text("\(answer)")
                    .font(
                        .custom(
                            "ArcadeClassic",
                            size: geometry.expand(
                                rect: cell.normalizedRect
                            )
                            .width / 2
                        )
                    )
                    .allowsHitTesting(false)
                    .shadow(color: Color(uiColor: .secondarySystemBackground), radius: 0, x: 3, y: 3)
            } else if !cell.noteDigits.isEmpty {
                // Note digits that the user has entered.
                Text("\(cell.noteDigitsString)")
                    .font(
                        .custom(
                            "ArcadeClassic",
                            size: geometry.expand(
                                rect: cell.normalizedRect
                            )
                            .width / 4
                        )
                    )
                    .allowsHitTesting(false)
                    .shadow(color: Color(uiColor: .secondarySystemBackground), radius: 0, x: 3, y: 3)
                    .lineLimit(/*@START_MENU_TOKEN@*/3/*@END_MENU_TOKEN@*/)
                    .opacity(0.5)
                    .allowsHitTesting(false)
            }
            
            // Renders the highlighted boarder around active cell
            if cell.correctDigit != nil {
                // Cell contains a correct answer
                Rectangle()
                    .strokeBorder(
                        ColorShader.color(
                            source: .cell,
                            style: settingsManager.settings.hintShadingStyle,
                            opacity: 1.0
                        ),
                        // TODO: Scaling border thickness for major, minor, cell, highlight, etc...
                        // lineWidth: board.highlights.cellIndexes.contains(cell.boardIndex) ? 4 : 0,
                        lineWidth: board.highlights.cellIndexes.contains(cell.boardIndex) ? geometry.expand(rect: cell.normalizedRect).width * 0.1 : 0,
                        antialiased: false
                    )
            } else if let activeCellIndex = board.activeCellIndex, cell.boardIndex == activeCellIndex {
                // Cell has not been answered. How do we highlight?
                switch board.actionMode {
                case .digit:
                    switch cell.digitStatus {
                    case .cannotPlay:
                        Rectangle()
                            .strokeBorder(
                                .cyan,
                                lineWidth: 4,
                                antialiased: false
                            )
                    case .incorrect:
                        Rectangle()
                            .strokeBorder(
                                .red,
                                lineWidth: 4,
                                antialiased: false
                            )
                    case .correct:
                        Rectangle()
                            .strokeBorder(
                                .yellow,
                                lineWidth: 4,
                                antialiased: false
                            )
                    case .unplayed:
                        Rectangle()
                            .strokeBorder(
                                .orange,
                                lineWidth: 4,
                                antialiased: false
                            )
                    }
//                    Rectangle()
//                        .strokeBorder(
//                            .orange,
//                            lineWidth: 4,
//                            antialiased: false
//                        )
                        
                case .notes:
                    Rectangle()
                        .strokeBorder(
                            .yellow,
                            lineWidth: 4,
                            antialiased: false
                        )
                #warning(
                    """
                    FIXME: @zakkhoyt - Add a case for when the user enters teh wrong answer.
                    Search logs for: "incorrect"
                    We need the result from `GameScreen board.play(digit:at:)`
                    """
                )
                    
                #warning(
                    """
                    FIXME: @zakkhoyt - Add a case for when the user enters an unplayable number.
                    Search logs for: "cannotPlay"
                    """
                )
                }
            }
        }
        .frame(
            width: geometry.expand(rect: cell.normalizedRect).width,
            height: geometry.expand(rect: cell.normalizedRect).height
        )
        .position(
            x: geometry.expand(rect: cell.normalizedRect).origin.x,
            y: geometry.expand(rect: cell.normalizedRect).origin.y
        )
        .offset(
            x: geometry.expand(rect: cell.normalizedRect).width / 2,
            y: geometry.expand(rect: cell.normalizedRect).height / 2
        )
    }
}

struct CellView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geometry in
            CellView(
                board: .previewBoard,
                settingsManager: SettingsManager.shared,
                cell: Board.previewBoard.cells[4],
                geometry: geometry
            )
        }
    }
}

extension Cell {
    // TODO: newline every 3 digits, center justified.
    var noteDigitsString: String {
        noteDigits.sorted()
            .map { "\($0)" }
            .joined(separator: " ")
    }
}
