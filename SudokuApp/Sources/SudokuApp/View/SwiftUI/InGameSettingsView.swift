//
//  SettingsView.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 8/18/22.
//  Copyright © 2022 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import SwiftUI

struct InGameSettingsView: View {
    @ObservedObject var board: Board
    @ObservedObject var settingsManager: SettingsManager
    @State var didTapExitButton: IndexTapped = { _ in }
    
    var actionModeProxy: Binding<Bool> {
        Binding<Bool>(
            get: {
                board.actionMode == .notes
            },
            set: {
                board.actionMode = $0 == true ? .notes : .digit
            }
        )
    }
    
    var body: some View {
        VStack {
            HStack(spacing: 0) {
                VStack(spacing: 0) {
                    Text("Difficulty")
                    Picker("Difficulty", selection: $settingsManager.settings.difficulty) {
                        ForEach(Difficulty.allCases) { difficulty in
                            Text(difficulty.title)
                                .tag(difficulty)
                                .font(.custom("ArcadeClassic", size: 20))
                        }
                    }
                    .pickerStyle(.menu)
                    .font(.custom("ArcadeClassic", size: 20))
                }
                
                VStack(spacing: 0) {
                    Text("Hint Style")
                    Picker("Shading Style", selection: $settingsManager.settings.hintShadingStyle) {
                        ForEach(Settings.HintShadingStyle.allCases) {
                            Text($0.title)
                                .tag($0)
                                .font(.custom("ArcadeClassic", size: 20))
                        }
                    }
                    .pickerStyle(.menu)
                    .font(.custom("ArcadeClassic", size: 20))
                }
                
                VStack(spacing: 0) {
                    Text("Hint Style")
                    Picker("Blend Mode", selection: $settingsManager.settings.hintBlendMode) {
                        ForEach(Settings.HintBlendMode.allCases) { blendMode in
                            Text(blendMode.title)
                                .tag(blendMode)
                                .font(.custom("ArcadeClassic", size: 20))
                        }
                    }
                    .pickerStyle(.menu)
                    .font(.custom("ArcadeClassic", size: 20))
                }
            }
            VStack(spacing: 0) {
                Text("Highlight Alpha: \(String(format: "%.2f", settingsManager.settings.hintAlpha))")
                Slider(
                    value: $settingsManager.settings.hintAlpha,
                    in: 0.0...1.0
                ) {
                    Text("Alpha")
                }
            }
            HStack(spacing: 0) {
                ForEach(HighlightStyle.allCases) { hightlightStyle in
                    Button(hightlightStyle.description) {
                        print("Toggle hightlightStyle: \(hightlightStyle.description)")
                        #warning("FIXME: @zakkhoyt this shoudln't be done in a SwiftUI view. Make an object.")
                        settingsManager.settings.toggleHighlightStyle(hightlightStyle)
                        board.highlightStyles = settingsManager.settings.highlightStyles
                    }
                    .foregroundColor(settingsManager.settings.highlightStyles.contains(hightlightStyle) ? .green : .green)
                    .tint(settingsManager.settings.highlightStyles.contains(hightlightStyle) ? .green.opacity(0.25) : .clear)
                    .buttonStyle(.borderedProminent)
                    .frame(maxWidth: .infinity) // HERE 1)
                    .font(.custom("ArcadeClassic", size: 12))
                }
            }
            HStack {
                Button("Quit") {
                    print("quit tapped")
                    didTapExitButton(0)
                }
                // .foregroundColor(settingsManager.settings.highlightStyles.contains(hightlightStyle) ? Color(uiColor: .systemBackground) : .green)
                .foregroundColor(.green)
                .tint(.clear)
                .buttonStyle(.borderedProminent)
//                .frame(maxWidth: .infinity) // HERE 1)
                .font(.custom("ArcadeClassic", size: 12))
                .padding(.horizontal)

                Spacer()
                Toggle("Notes", isOn: actionModeProxy)
                    .padding(.horizontal)
                    .toggleStyle(.button)
                    .tint(.green)
                    .font(.custom("ArcadeClassic", size: 12))
            }
        }
    }
}

struct InGameSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            InGameSettingsView(
                board: Board.previewBoard,
                settingsManager: SettingsManager.shared
            )
            .preferredColorScheme(.light)
            .previewInterfaceOrientation(.portrait)
            
            InGameSettingsView(
                board: Board.previewBoard,
                settingsManager: SettingsManager.shared
            )
            .preferredColorScheme(.dark)
            .previewInterfaceOrientation(.portrait)
        }
    }
}
