//
//  SceneDelegateCoadjutor.swift
//
//  Created by Zakk Hoyt on 10/1/22.
//

import UIKit

public protocol SceneDelegateCoadjutable: UIWindowSceneDelegate {
    var window: UIWindow? { get set }
}

public class SceneDelegateCoadjutor: NSObject, SceneDelegateCoadjutable {
    // MARK: Implements SceneDelegateCoadjutable
    
    public var window: UIWindow? {
        get { sceneDelegate?.window }
        set { sceneDelegate?.window = newValue }
    }
    
    // MARK: Public vars
    
    public static let shared = SceneDelegateCoadjutor()
    
    /// This should only be set in the App's SceneDelegate.willConnectTo...
    public var sceneDelegate: SceneDelegateCoadjutable?
}

// MARK: - Implements UIWindowSceneDelegate

extension SceneDelegateCoadjutor {
    public func windowScene(
        _ windowScene: UIWindowScene,
        didUpdate previousCoordinateSpace: UICoordinateSpace,
        interfaceOrientation previousInterfaceOrientation: UIInterfaceOrientation,
        traitCollection previousTraitCollection: UITraitCollection
    ) {
        // Called when the coordinate space, interface orientation, or trait collection of a UIWindowScene changes
        // Always called when a UIWindowScene moves between screens
    }
    
    public func windowScene(
        _ windowScene: UIWindowScene,
        performActionFor shortcutItem: UIApplicationShortcutItem,
        completionHandler: @escaping (Bool) -> Void
    ) {
        // Called when the user activates your application by selecting a shortcut on the home screen,
        // and the window scene is already connected.
        completionHandler(true)
    }

//    public func windowScene(
//        _ windowScene: UIWindowScene,
//        performActionFor shortcutItem: UIApplicationShortcutItem
//    ) async -> Bool {
//        true
//    }
    
//    // Called after the user indicates they want to accept a CloudKit sharing invitation in your application
//    // and the window scene is already connected.
//    // You should use the CKShareMetadata object's shareURL and containerIdentifier to schedule a CKAcceptSharesOperation, then start using
//    // the resulting CKShare and its associated record(s), which will appear in the CKContainer's shared database in a zone matching that of the record's owner.
//    public func windowScene(
//        _ windowScene: UIWindowScene,
//        userDidAcceptCloudKitShareWith cloudKitShareMetadata: CKShareMetadata
//    ) {
//
//    }
}

// MARK: - Implements UISceneDelegate

extension SceneDelegateCoadjutor {
    public func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        let storyboard = UIStoryboard(name: "Start", bundle: .module)
        let viewController = storyboard.instantiateInitialViewController() as? StartViewController
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        sceneDelegate?.window = window
    }
    
    public func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }
    
    public func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }
    
    public func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }
        
    public func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }
    
    public func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }
    
    public func stateRestorationActivity(
        for scene: UIScene
    ) -> NSUserActivity? {
        // This is the NSUserActivity that will be used to restore state when the Scene reconnects.
        // It can be the same activity used for handoff or spotlight, or it can be a separate activity
        // with a different activity type and/or userInfo.
        // After this method is called, and before the activity is actually saved in the restoration file,
        // if the returned NSUserActivity has a delegate (NSUserActivityDelegate), the method
        // userActivityWillSave is called on the delegate. Additionally, if any UIResponders
        // have the activity set as their userActivity property, the UIResponder updateUserActivityState
        // method is called to update the activity. This is done synchronously and ensures the activity
        // has all info filled in before it is saved.
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
        return nil
    }

    public func scene(
        _ scene: UIScene,
        restoreInteractionStateWith stateRestorationActivity: NSUserActivity
    ) {
        // This will be called after scene connection, but before activation, and will provide the
        // activity that was last supplied to the stateRestorationActivityForScene callback, or
        // set on the UISceneSession.stateRestorationActivity property.
        // Note that, if it's required earlier, this activity is also already available in the
        // UISceneSession.stateRestorationActivity at scene connection time.
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }

    public func scene(
        _ scene: UIScene,
        willContinueUserActivityWithType userActivityType: String
    ) {
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }

    public func scene(
        _ scene: UIScene,
        continue userActivity: NSUserActivity
    ) {
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }

    public func scene(
        _ scene: UIScene,
        didFailToContinueUserActivityWithType userActivityType: String,
        error: Error
    ) {
        print("Function called")
    }

    public func scene(
        _ scene: UIScene,
        didUpdate userActivity: NSUserActivity
    ) {
        print("ApplicationState: \(UIApplication.shared.applicationState.description)")
    }
}

extension UIApplication.State: CustomStringConvertible {
    public var description: String {
        switch self {
        case .active: return "Active"
        case .inactive: return "Inactive"
        case .background: return "Background"
        @unknown default: return "Unknown"
        }
    }
}
