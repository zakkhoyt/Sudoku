//
//  AppDelegateCoadjutor.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 6/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import SudokuKit
import UIKit

public protocol AppDelegateCoadjutable: UIApplicationDelegate {
    var window: UIWindow? { get set }
}

public class AppDelegateCoadjutor: UIResponder, AppDelegateCoadjutable {
    // MARK: Implements AppDelegateCoadjutable
    
    public var window: UIWindow? {
        get { appDelegate?.window }
        set { appDelegate?.window = newValue }
    }
    
    public static let shared = AppDelegateCoadjutor()
    
    public var appDelegate: AppDelegateCoadjutable?

    public func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        print("documents dir: " + Utilities.documentsDirURL.absoluteString)
        print("appGroup dir: " + Utilities.appGroupDirURL.absoluteString)
        Appearance.setup()
        _ = AudioController.sharedInstance
        return true
    }
}
