//
//  UIScene+Helpers.swift
//  Nightlight
//
//  Created by Zakk Hoyt on 7/16/21.
//  Copyright © 2021 Hatch. All rights reserved.
//

import UIKit

extension UIResponder {
    @objc
    public var scene: UIScene? { nil }
}

extension UIScene {
    @objc
    override public var scene: UIScene? { self }
}

extension UIView {
    @objc
    override public var scene: UIScene? {
        window?.windowScene ?? next?.scene
    }
}

extension UIViewController {
    @objc
    override public var scene: UIScene? {
        next?.scene
            ?? parent?.scene
            ?? presentingViewController?.scene
    }
}

extension UIApplication {
    /// https://stackoverflow.com/a/56589151/803882
    public var window: UIWindow? {
        connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .compactMap { $0 as? UIWindowScene }
            .first?.windows
            .filter { $0.isKeyWindow }
            .first
    }
}
