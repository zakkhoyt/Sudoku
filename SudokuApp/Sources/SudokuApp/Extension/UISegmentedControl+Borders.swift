//
//  UISegmentedControl+Borders.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/11/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

extension UISegmentedControl {
    func removeBorders() {
        let normalImage = imageWithColor(color: .gameBackgroundColor)
        let selectedImage = imageWithColor(color: .tint)
        let clearImage = imageWithColor(color: .clear)

        setBackgroundImage(normalImage, for: .normal, barMetrics: .default)
        setBackgroundImage(selectedImage, for: .selected, barMetrics: .default)
        setDividerImage(clearImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    // create a 1x1 image with this color
    func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fillEllipse(in: rect)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            preconditionFailure("Failed to get image from context")
        }
        UIGraphicsEndImageContext()
        return image
    }
}
