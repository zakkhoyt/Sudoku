//
//  UINavigationBar+Height.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/11/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit

extension UINavigationBar {
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        CGSize(width: frame.size.width, height: 64)
    }
}
