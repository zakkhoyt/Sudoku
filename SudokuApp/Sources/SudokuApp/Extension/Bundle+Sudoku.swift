//
//  Bundle+Sudoku.swift
//  Sudoku-iOS
//
//  Created by Zakk Hoyt on 7/11/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import CoreText
import Foundation

extension Bundle {
    class func AudioSamplesBundle() -> Bundle? {
        guard let bundlePath = Bundle.module.path(forResource: "AudioSamples", ofType: "bundle"),
              let bundle = Bundle(path: bundlePath) else {
            return nil
        }
        return bundle
    }
}
