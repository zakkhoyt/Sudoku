// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// swiftlint:disable prefixed_toplevel_constant

let package = Package(
    name: "SudokuApp",
    platforms: [
        .iOS(.v15),
        .macOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SudokuApp",
            targets: ["SudokuApp"]
        )
    ],
    dependencies: [
        .package(
            name: "SudokuKit",
            path: "../SudokuKit"
        ),

        // https://github.com/realm/SwiftLint/releases
        .package(
            url: "https://github.com/realm/SwiftLint", 
            // exact: Version(0, 50, 3)
            // exact: Version(0, 50, 1)
            // exact: Version(0, 49, 1)
            // https://github.com/realm/SwiftLint/issues/4558
            revision: "0.50.3"
        ),
        // https://github.com/nicklockwood/SwiftFormat/releases
        .package(
            url: "https://github.com/nicklockwood/SwiftFormat", 
            exact: Version(0, 50, 9)
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SudokuApp",
            dependencies: [
                .product(
                    name: "SudokuKit",
                    package: "SudokuKit"
                )
            ],
            exclude: [
                "Document/"
            ],
            resources: [
                .copy("Resources/AudioSamples.bundle"),
                .copy("Resources/Fonts.bundle"),
                .process("Resources/Assets.xcassets"),
                .process("ViewController/About/About.storyboard"),
                .process("ViewController/Game/Game.storyboard"),
                .process("ViewController/Help/Help.storyboard"),
                .process("ViewController/PopupMenu/PopupMenu.storyboard"),
                .process("ViewController/Settings/Settings.storyboard"),
                .process("ViewController/Start/Start.storyboard")
            ],
            plugins: [
            //     // https://github.com/realm/SwiftLint/tree/main/Plugins/SwiftLintPlugin
            //    .plugin(
            //         name: "SwiftLintPlugin", 
            //         package: "SwiftLint"
            //     ),
            //     // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
            //     .plugin(
            //         name: "SwiftFormatPlugin",
            //         package: "SwiftFormat"
            //     ),
//                // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
//                .plugin(
//                    name: "SwiftFormatPluginXcode",
//                    package: "SwiftFormat"
//                )
            ]
        ),
        .testTarget(
            name: "SudokuAppTests",
            dependencies: [
                "SudokuApp"
            ]
        )
    ]
)
// swiftlint:enable prefixed_toplevel_constant
