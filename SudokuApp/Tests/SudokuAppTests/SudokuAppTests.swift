@testable import SudokuApp
import XCTest

final class SudokuAppTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SudokuApp().text, "Hello, World!")
    }
}
