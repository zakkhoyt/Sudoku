// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DrawingUI",
    platforms: [
        .iOS(.v15),
        .macOS(.v12)
    ],
    products: [
        .library(
            name: "BaseUtility",
            targets: ["BaseUtility"]
        ),
        .library(
            name: "DrawingUI",
            targets: ["DrawingUI"]
        ),
        .plugin(
            name: "MySwiftformatLintingPlugin",
            targets: ["MySwiftformatLintingPlugin"]
        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),

        // // https://github.com/realm/SwiftLint/releases
        // .package(
        //     url: "https://github.com/realm/SwiftLint",
        //     // exact: Version(0, 50, 3)
        //     // exact: Version(0, 50, 1)
        //     // exact: Version(0, 49, 1)
        //     // https://github.com/realm/SwiftLint/issues/4558
        //     revision: "0.50.3"
        // ),

        // https://github.com/nicklockwood/SwiftFormat/releases
        .package(
            url: "https://github.com/nicklockwood/SwiftFormat",
            exact: Version(0, 50, 9)
        )
    ],
    targets: [
        
        .target(
            name: "BaseUtility",
            dependencies: [],
//            swiftSettings: swiftSettings,
            plugins: []
        ),
        .testTarget(
            name: "BaseUtilityTests",
            dependencies: ["BaseUtility"]
//            swiftSettings: swiftSettings
        ),

        .target(
            name: "DrawingUI",
            dependencies: [
                "BaseUtility"
            ],
            exclude: [
            ],
//            swiftSettings: swiftSettings,
            plugins: [
                    //     // https://github.com/realm/SwiftLint/tree/main/Plugins/SwiftLintPlugin
                //    .plugin(
                //         name: "SwiftLintPlugin",
                //         package: "SwiftLint"
                //     ),
                //     // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
                //     .plugin(
                //         name: "SwiftFormatPlugin",
                //         package: "SwiftFormat"
                //     ),
    //                // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
    //                .plugin(
    //                    name: "SwiftFormatPluginXcode",
    //                    package: "SwiftFormat"
    //                )
                ]
        ),
//        .target(
//            name: "DrawingUI",
//            dependencies: [],
//            plugins: [
//                //     // https://github.com/realm/SwiftLint/tree/main/Plugins/SwiftLintPlugin
//            //    .plugin(
//            //         name: "SwiftLintPlugin",
//            //         package: "SwiftLint"
//            //     ),
//            //     // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
//            //     .plugin(
//            //         name: "SwiftFormatPlugin",
//            //         package: "SwiftFormat"
//            //     ),
////                // https://github.com/nicklockwood/SwiftFormat/tree/master/Plugins/SwiftFormatPlugin
////                .plugin(
////                    name: "SwiftFormatPluginXcode",
////                    package: "SwiftFormat"
////                )
//            ]
//        ),
        .testTarget(
            name: "DrawingUITests",
            dependencies: ["DrawingUI"]
        ),
        // .plugin(
        //     name: "MySwiftformatLintingPlugin",
        //     capability: .command(
        //         //               intent: .sourceCodeFormatting(),
        //        intent: .custom(verb: "sf-lint", description: "Lint using swiftformat"),
        //         permissions: [
        //             .writeToPackageDirectory(reason: "This command reformats source files")
        //         ]
        //     ),
        //     dependencies: [
        //         .product(
        //             name: "swiftformat",
        //             package: "SwiftFormat"
        //         )
        //     ]
        // ),
        .plugin(
            name: "MySwiftformatLintingPlugin",
            capability: .buildTool(),
            dependencies: [
                .product(
                    name: "swiftformat",
                    package: "SwiftFormat"
                )
            ]
        )
    ]
)
