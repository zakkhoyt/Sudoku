// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import Foundation
import PackagePlugin

// @main
// struct SwiftformatPlugin: CommandPlugin {
//    func performCommand(context: PluginContext, arguments: [String]) throws {
//        let tool = try context.tool(named: "swiftformat")
//        let toolUrl = URL(fileURLWithPath: tool.path.string)
//
//        for target in context.package.targets {
//            print("target.directory: \(target.directory)")
//            guard let target = target as? SourceModuleTarget else { continue }
//
//            let process = Process()
//            process.executableURL = toolUrl
//            process.arguments = [
//                "\(target.directory)"
//                // "--in-process-sourcekit" // this line will fix the issues...
//            ]
//
//            try process.run()
//            process.waitUntilExit()
//
//            if process.terminationReason == .exit, process.terminationStatus == 0 {
//                print("Formatted the source code in \(target.directory).")
//            } else {
//                let problem = "\(process.terminationReason):\(process.terminationStatus)"
//                Diagnostics.error("swift-format invocation failed: \(problem)")
//            }
//        }
//    }
// }

///// Defines functionality for all plugins having a `buildTool` capability.
// public protocol BuildToolPlugin : PackagePlugin.Plugin {
//
//    /// Invoked by SwiftPM to create build commands for a particular target.
//    /// The context parameter contains information about the package and its
//    /// dependencies, as well as other environmental inputs.
//    ///
//    /// This function should create and return build commands or prebuild
//    /// commands, configured based on the information in the context. Note
//    /// that it does not directly run those commands.
//    func createBuildCommands(context: PackagePlugin.PluginContext, target: PackagePlugin.Target) async throws -> [PackagePlugin.Command]
// }

// @main
// struct SwiftformatPlugin: BuildToolPlugin {
//    func createBuildCommands(
//        context: PackagePlugin.PluginContext,
//        target: PackagePlugin.Target
//    ) async throws -> [PackagePlugin.Command] {
//        guard let sourceTarget = target as? SourceModuleTarget else {
//            return []
//        }
//
//        let inputFilePaths = sourceTarget
//            .sourceFiles(withSuffix: "swift")
//            .map(\.path)
//
//        guard inputFilePaths.isEmpty == false else {
//            // Don't lint anything if there are no Swift source files in this target
//            return []
//        }
//
//        let swiftformat = try context.tool(named: "swiftformat")
//        var arguments: [String] = [
////            "lint",
//        //    "--quiet",
//        //    "--cache-path", "\(context.pluginWorkDirectory)"
//
////            FORMAT_SWIFTLINT=$(echo $PREFIX ~/.mint/bin/swiftlint lint --fix --config .swiftlint.yml $QUIET $files)
////            FORMAT_SWIFTFORMAT=$(echo $PREFIX ~/.mint/bin/swiftformat $files --config .swiftformat  $QUIET)
////            LINT_SWIFTLINT=$(echo $PREFIX ~/.mint/bin/swiftlint lint --strict --reporter json --quiet .)
////            LINT_SWIFTFORMAT=$(echo $PREFIX ~/.mint/bin/swiftformat . --config .swiftformat --report fastlane/.gitignored/swiftformat_lint_output.json --lint --quiet)
//
//
//            // swiftformat . --config .swiftformat --report fastlane/.gitignored/swiftformat_lint_output.json --lint --quiet
//            // "--lint",
//            // "--quiet",
//            // "--cache-path", "\(context.pluginWorkDirectory)"
//
//
//        ]
//
//        // Manually look for configuration files, to avoid issues when the plugin does not execute our tool from the
//        // package source directory.
//        if let configuration = context.package.directory.firstConfigurationFileInParentDirectories() {
//            arguments.append(contentsOf: [
//                "--config", "\(configuration.string)"
//            ])
//        }
//
//        arguments += inputFilePaths.map(\.string)
//
//        return [
//            .buildCommand(
//                displayName: "--SwiftFormat",
//                executable: swiftformat.path,
//                arguments: arguments,
//                inputFiles: inputFilePaths,
//                outputFiles: [context.pluginWorkDirectory]
//            )
//        ]
//    }
// }
//

// @main
// struct SwiftformatPlugin: CommandPlugin {

//     func performCommand(
//         context: PackagePlugin.PluginContext,
//         arguments: [String]
//     ) async throws {
//         let tool = try context.tool(named: "swiftformat")
//         let toolUrl = URL(fileURLWithPath: tool.path.string)
        
//         for target in context.package.targets {
//             print("target.directory: \(target.directory)")
//             guard let target = target as? SourceModuleTarget else { continue }

//             let process = Process()
//             process.executableURL = toolUrl
//             process.arguments = [
//                 "."
//             ]
            
//             try process.run()
//             process.waitUntilExit()
            
//             if process.terminationReason == .exit, process.terminationStatus == 0 {
//                 print("Formatted the source code in \(target.directory).")
//             } else {
//                 let problem = "\(process.terminationReason):\(process.terminationStatus)"
//                 Diagnostics.error("swiftformat invocation failed: \(problem)")
//             }
//         }
//     }
// }


@main
struct SwiftformatLinterPlugin: BuildToolPlugin {
    func createBuildCommands(
        context: PackagePlugin.PluginContext,
        target: PackagePlugin.Target
    ) async throws -> [PackagePlugin.Command] {
        guard let sourceTarget = target as? SourceModuleTarget else {
            return []
        }

        let inputFilePaths = sourceTarget.sourceFiles(withSuffix: "swift")
            .map(\.path)

        guard inputFilePaths.isEmpty == false else {
            // Don't lint anything if there are no Swift source files in this target
            return []
        }

        let swiftformat = try context.tool(named: "swiftformat")
        var arguments: [String] = [
            // "lint",
            // "--quiet",
            // "--cache-path", "\(context.pluginWorkDirectory)"
            // "."
        ]

        // Manuallsy look for configuration files, to avoid issues when the plugin does not execute our tool from the
        // package source directory.
        if let configuration = context.package.directory.firstConfigurationFileInParentDirectories() {
            arguments.append(contentsOf: [
                "--config", "\(configuration.string)"
            ])
        }

        arguments += inputFilePaths.map(\.string)

        return [
            .buildCommand(
                displayName: "SwiftFormat",
                executable: swiftformat.path,
                arguments: arguments,
                inputFiles: inputFilePaths,
                outputFiles: [context.pluginWorkDirectory]
            )
        ]
    }
}

#if canImport(XcodeProjectPlugin)
import XcodeProjectPlugin

extension SwiftformatLinterPlugin: XcodeBuildToolPlugin {
    func createBuildCommands(
        context: XcodePluginContext,
        target: XcodeTarget
    ) throws -> [Command] {
        let inputFilePaths = target.inputFiles
            .filter { $0.type == .source && $0.path.extension == "swift" }
            .map(\.path)

//        guard inputFilePaths.isEmpty == false else {
//            // Don't lint anything if there are no Swift source files in this target
//            return []
//        }

        let swiftformat = try context.tool(named: "swiftformat")
        var arguments: [String] = [
            // "lint",
            // "--quiet",
            // "--cache-path", "\(context.pluginWorkDirectory)"
//            "."
        ]

//        // Xcode build tool plugins don't seem to run from the project source directory, so our auto-discovery of
//        // configuration files doesn't work. We approximate it here.
//        if let configuration = context.xcodeProject.directory.firstConfigurationFileInParentDirectories() {
//            arguments.append(contentsOf: [
//                "--config", "\(configuration.string)"
//            ])
//        }

        arguments += inputFilePaths.map(\.string)

        return [
            .buildCommand(
                displayName: "--SwiftFormat--",
                executable: swiftformat.path,
                arguments: arguments,
                inputFiles: inputFilePaths,
                outputFiles: [context.pluginWorkDirectory]
            )
        ]
    }
}
#endif
