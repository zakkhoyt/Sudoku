@testable import DrawingUI
import XCTest

final class DrawingUIDummyTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DrawingUI().text, "Hello, World!")
    }
}
